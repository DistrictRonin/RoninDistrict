package de.ronin.ronindistrict;

import de.ostylk.baseapi.BaseAPI;
import de.ostylk.baseapi.modules.command.ModuleCommand;
import de.ostylk.baseapi.modules.concurrent.ModuleConcurrent;
import de.ostylk.baseapi.modules.config.ModuleConfig;
import de.ostylk.baseapi.modules.economy.Currency;
import de.ostylk.baseapi.modules.economy.ModuleEconomy;
import de.ostylk.baseapi.modules.item.ModuleItem;
import de.ostylk.baseapi.modules.playercache.ModulePlayerCache;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ronin.ronindistrict.holder.plot.command.PlotChunkHolderArgument;
import de.ronin.ronindistrict.holder.spawn.SpawnChunkHolderArgument;
import de.ronin.ronindistrict.level.LevelCache;
import de.ronin.ronindistrict.plot.PlotBuyProcessor;
import de.ronin.ronindistrict.plot.PlotProtectionBuyProcessor;
import de.ronin.ronindistrict.world.DistrictWorldCache;
import de.ronin.ronindistrict.world.DistrictWorldProtector;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutionException;

public class RoninDistrict extends JavaPlugin {
    
    public static ModuleConfig CONFIG;
    public static ModuleConcurrent CONCURRENT;
    public static ModuleCommand COMMAND;
    public static ModuleEconomy ECONOMY;
    public static ModuleItem ITEM;
    public static ModulePlayerCache PLAYER_CACHE;
    public static ModuleWindow WINDOW;
    
    public static Currency ROLARI;
    
    @Getter private DistrictWorldCache worldCache;
    @Getter private LevelCache levelCache;
    
    @Override
    public void onLoad() {
        BaseAPI baseAPI = getServer().getServicesManager().getRegistration(BaseAPI.class).getProvider();
        CONFIG = baseAPI.getModule(ModuleConfig.class);
        CONCURRENT = baseAPI.getModule(ModuleConcurrent.class);
        COMMAND = baseAPI.getModule(ModuleCommand.class);
        ECONOMY = baseAPI.getModule(ModuleEconomy.class);
        ITEM = baseAPI.getModule(ModuleItem.class);
        PLAYER_CACHE = baseAPI.getModule(ModulePlayerCache.class);
        WINDOW = baseAPI.getModule(ModuleWindow.class);
        
        CONFIG.getSettingManager(this).loadSettings(PlotBuyProcessor.class);
        CONFIG.getSettingManager(this).loadSettings(PlotProtectionBuyProcessor.class);
        CONFIG.getSettingManager(this).loadSettings(PlotChunkHolderArgument.class);
        CONFIG.getSettingManager(this).loadSettings(SpawnChunkHolderArgument.class);
        
        COMMAND.scanPlugin(this);
    }
    
    @Override
    public void onEnable() {
        ROLARI = ECONOMY.getCurrency("ronin_main");
        
        saveResource("levels.yml", false);
        
        this.worldCache = new DistrictWorldCache(CONFIG.loadConfig(this, "worlds"));
        getServer().getPluginManager().registerEvents(new DistrictWorldProtector(this.worldCache), this);
        
        this.levelCache = new LevelCache(CONFIG.loadConfig(this, "levels"), this.worldCache);
        
        COMMAND.finishPlugin(this, "ronindistrict.command");
    }
    
    @Override
    public void onDisable() {
        try {
            System.out.println("Shutting down RoninDistrict...");
            this.worldCache.shutdown().get();
            System.out.println("RoninDistrict shut down.");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    public static RoninDistrict getInstance() {
        return RoninDistrict.getPlugin(RoninDistrict.class);
    }
}
