package de.ronin.ronindistrict.world;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkHolderIO;
import de.ronin.ronindistrict.holder.ChunkHolderMap;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.holder.restore.RestoreChunkHolder;
import de.ronin.ronindistrict.holder.server.ServerChunkHolder;
import de.ronin.ronindistrict.holder.spawn.SpawnChunkHolder;
import de.ronin.ronindistrict.plot.PlotBuyProcessor;
import de.ronin.ronindistrict.plot.PlotCostHUD;
import de.ronin.ronindistrict.world.archive.ChunkArchive;
import de.ronin.ronindistrict.world.price.LazyChunkPriceCache;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.Delegate;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@FieldDefaults(makeFinal = true)
public class DistrictWorld {
    
    @Delegate(types = World.class) private World world;
    @Getter private Map<ChunkPointer, ChunkHolder> holders;
    
    @Getter private ChunkArchive archive;
    
    @Getter private RegionManager regionManager;
    
    @Getter private ChunkHolderIO holderIO;
    @Getter @NonFinal private ServerChunkHolder serverChunkHolder;
    @Getter @NonFinal private RestoreChunkHolder restoreChunkHolder;
    @Getter @NonFinal private ChunkHolderMap<String, SpawnChunkHolder> spawnChunkHolder;
    @Getter @NonFinal private ChunkHolderMap<UUID, PlotChunkHolder> plotChunkHolder;
    @Getter @NonFinal private ChunkHolderMap<String, ProtectionChunkHolder> protectionChunkHolder;
    
    @Getter private LazyChunkPriceCache chunkPriceCache;
    @Getter private PlotBuyProcessor calculator;
    private PlotCostHUD costHUD;
    
    public DistrictWorld(UUID worldUUID) {
        this.world = Bukkit.getWorld(worldUUID);
        this.holders = new HashMap<>();
        this.archive = new ChunkArchive(this);
        assert(this.world != null);
    
        val container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        this.regionManager = container.get(BukkitAdapter.adapt(this.world));
        this.setupWorldGuard();
        
        this.holderIO = new ChunkHolderIO(this);
        RoninDistrict.CONCURRENT.submitTask(this::loadChunkHolders);
        
        this.chunkPriceCache = new LazyChunkPriceCache(this);
        this.calculator = new PlotBuyProcessor(this, this.chunkPriceCache);
        this.costHUD = new PlotCostHUD(this, this.calculator);
        Bukkit.getPluginManager().registerEvents(this.costHUD, RoninDistrict.getInstance());
    }
    
    private void setupWorldGuard() {
        val region = this.regionManager.getRegion("__global__");
        assert(region != null);
        region.setFlag(Flags.BUILD, StateFlag.State.DENY);
    }
    
    @SneakyThrows
    private <T extends ChunkHolder> T initSingleHolder(String id, Class<T> type, Supplier<T> def) {
        if (this.holderIO.hasData(id)) {
            return this.holderIO.load(type, id).get();
        } else {
            val holder = def.get();
            this.holderIO.register(holder);
            return holder;
        }
    }
    
    private void loadChunkHolders() {
        this.serverChunkHolder = initSingleHolder(ServerChunkHolder.ID, ServerChunkHolder.class,
                () -> new ServerChunkHolder(this));
        
        this.restoreChunkHolder = initSingleHolder(RestoreChunkHolder.ID, RestoreChunkHolder.class,
                () -> new RestoreChunkHolder(this));
    
        this.spawnChunkHolder = new ChunkHolderMap<>(this.holderIO, SpawnChunkHolder.class,
                (file, name) -> name.startsWith(SpawnChunkHolder.ID),
                SpawnChunkHolder::getName);
        
        this.plotChunkHolder = new ChunkHolderMap<>(this.holderIO, PlotChunkHolder.class,
                (file, name) -> name.startsWith(PlotChunkHolder.ID),
                holder -> holder.getPlot().getOwner());
        
        this.protectionChunkHolder = new ChunkHolderMap<>(this.holderIO, ProtectionChunkHolder.class,
                (file, name) -> name.startsWith(ProtectionChunkHolder.ID),
                holder -> holder.getTarget().getId());
    }
    
    public CompletableFuture<Void> assignHolder(ChunkPointer pointer, ChunkHolder holder) {
        CompletableFuture<Void> future;
        if (!this.archive.isArchived(pointer.getChunkX(), pointer.getChunkZ())) {
            future = this.archive.archive(pointer.getChunkX(), pointer.getChunkZ());
        } else {
            future = CompletableFuture.supplyAsync(() -> null);
        }
        
        return future.thenApply(a -> {
            synchronized (this.holders) {
                val previousHolder = this.holders.get(pointer);
                if (previousHolder != null) {
                    previousHolder.unregisterChunk(pointer);
                }
                this.holders.put(pointer, holder);
                holder.registerChunk(pointer);
            }
            
            this.chunkPriceCache.invalidateChunkPrices();
            return null;
        });
    }
    
    /** only call if you know what you're doing */
    public CompletableFuture<Void> deassignHolder(ChunkPointer pointer) {
        return this.archive.restore(pointer.getChunkX(), pointer.getChunkZ()).thenApply(a -> {
            synchronized (this.holders) {
                val previousHolder = this.holders.get(pointer);
                if (previousHolder != null) {
                    previousHolder.unregisterChunk(pointer);
                }
                this.holders.remove(pointer);
            }
            
            this.chunkPriceCache.invalidateChunkPrices();
            return null;
        });
    }
    
    public CompletableFuture<Void> queueDeassignment(ChunkPointer pointer) {
        return this.assignHolder(pointer, this.restoreChunkHolder);
    }
    
    public Chunk getChunkAt(ChunkPointer pointer) {
        return this.world.getChunkAt(pointer.getChunkX(), pointer.getChunkZ());
    }
}
