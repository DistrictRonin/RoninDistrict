package de.ronin.ronindistrict.world;

import de.ronin.ronindistrict.holder.ChunkPointer;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockPlaceEvent;

@FieldDefaults(makeFinal = true)
public class DistrictWorldProtector implements Listener {

    private DistrictWorldCache worldCache;
    
    public DistrictWorldProtector(DistrictWorldCache worldCache) {
        this.worldCache = worldCache;
    }
    
    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        this.handleBlockEvent(e);
    }
    
    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        this.handleBlockEvent(e);
    }
    
    public <T extends BlockEvent & Cancellable> void handleBlockEvent(T e) {
        val world = this.worldCache.getWorlds().get(e.getBlock().getWorld().getUID());
        if (world != null) {
            val chunk = e.getBlock().getChunk();
            ChunkPointer pointer = new ChunkPointer(chunk.getX(), chunk.getZ());
            if (!world.getHolders().containsKey(pointer)) {
                e.setCancelled(true);
            }
        }
    }
}
