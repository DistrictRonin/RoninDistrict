package de.ronin.ronindistrict.world.archive;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import org.bukkit.block.data.BlockData;

@FieldDefaults(makeFinal = true)
public class BlockDataDictionary {

    @Getter private BiMap<Integer, BlockData> dictionary;
    
    @NonFinal private int reverseCurrent = 0;
    
    public BlockDataDictionary() {
        this.dictionary = HashBiMap.create();
    }
    
    public void add(int id, BlockData data) {
        this.dictionary.put(id, data);
    }
    
    public BlockData lookup(int id) {
        return this.dictionary.get(id);
    }
    
    public int lookup(BlockData data) {
        if (!this.dictionary.inverse().containsKey(data)) {
            this.dictionary.put(this.reverseCurrent++, data);
        }
        
        return this.dictionary.inverse().get(data);
    }
}
