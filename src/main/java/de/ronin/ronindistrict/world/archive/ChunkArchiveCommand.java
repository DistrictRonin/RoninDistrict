package de.ronin.ronindistrict.world.archive;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.entity.Player;

@FieldDefaults(makeFinal = true)
public class ChunkArchiveCommand {
    
    @Command("district.archive.save")
    public void saveChunk(Player player, @Optional("$self") DistrictWorld world) {
        val chunkX = player.getLocation().getChunk().getX();
        val chunkZ = player.getLocation().getChunk().getZ();
        val future = world.getArchive().archive(chunkX, chunkZ);
        player.sendMessage("\u00a7aArchivierung von Chunk " + chunkX + "|" + chunkZ + " beginnt.");
        future.thenRun(() -> {
            player.sendMessage("\u00a7aChunk erfolgreich archiviert.");
        });
    }
    
    @Command("district.archive.restore")
    public void restoreChunk(Player player, @Optional("$self") DistrictWorld world) {
        val chunkX = player.getLocation().getChunk().getX();
        val chunkZ = player.getLocation().getChunk().getZ();
        val future = world.getArchive().restore(chunkX, chunkZ);
        player.sendMessage("\u00a7aChunk " + chunkX + "|" + chunkZ + " wird nun wiederhergestellt.");
        future.thenRun(() -> {
            player.sendMessage("\u00a7aChunk wurde wiederhergestellt.");
        });
    }
}
