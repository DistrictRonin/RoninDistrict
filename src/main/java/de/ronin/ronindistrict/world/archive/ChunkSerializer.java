package de.ronin.ronindistrict.world.archive;

import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.val;
import lombok.var;
import org.bukkit.Chunk;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Material;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

@FieldDefaults(makeFinal = true)
public class ChunkSerializer {
    
    protected static final int SET_BIT = 0b1000_0000_0000_0000_0000_0000_0000_0000;
    
    @SneakyThrows
    private static byte[] writeBlocks(ChunkSnapshot chunk, BlockDataDictionary dictionary) {
        val data = new ByteArrayOutputStream();
        val out = new DataOutputStream(data);
    
        var skip = false;
        for (int y = 0; y < 256; y++) {
            for (int z = 0; z < 16; z++) {
                for (int x = 0; x < 16; x++) {
                    if (chunk.getBlockType(x, y, z) == Material.AIR) {
                        skip = true;
                        continue;
                    }
                    
                    val blockData = chunk.getBlockData(x, y, z);
                    val id = dictionary.lookup(blockData);
                    
                    if (skip) {
                        out.writeInt(x | SET_BIT);
                        out.writeInt(y);
                        out.writeInt(z);
                        skip = false;
                    }
                    
                    out.writeInt(id);
                }
            }
        }
        
        return data.toByteArray();
    }
    
    @SneakyThrows
    public static void serialize(BufferedOutputStream writer, Chunk chunk) {
        val out = new DataOutputStream(writer);
    
        val snapshot = chunk.getChunkSnapshot(false, false, false);
    
        val dictionary = new BlockDataDictionary();
        val blockData = writeBlocks(snapshot, dictionary);
        
        out.writeInt(dictionary.getDictionary().size());
        for (Integer id : dictionary.getDictionary().keySet()) {
            val data = dictionary.lookup(id);
            out.writeInt(id);
            out.writeUTF(data.getAsString());
        }
        out.write(blockData);
        out.flush();
    }
}
