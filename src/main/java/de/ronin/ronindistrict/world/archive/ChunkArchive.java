package de.ronin.ronindistrict.world.archive;

import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.io.*;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true)
public class ChunkArchive {

    private static final String FILE_FORMAT = "Chunk_%s_%s.arc";
    
    private DistrictWorld world;
    private File archiveFolder;
    
    public ChunkArchive(DistrictWorld world) {
        this.world = world;
        this.archiveFolder = new File(world.getWorldFolder(), "RoninDistrict/archive");
        this.archiveFolder.mkdirs();
    }
    
    @SneakyThrows
    public CompletableFuture<Void> archive(int chunkX, int chunkZ) {
        File archive = new File(this.archiveFolder, String.format(FILE_FORMAT, chunkX, chunkZ));
        if (!archive.exists()) {
            archive.createNewFile();
        }
        
        val writer = new BufferedOutputStream(new FileOutputStream(archive, false));
        val chunk = this.world.getChunkAt(chunkX, chunkZ);
        
        return CompletableFuture.supplyAsync(() -> {
            try {
                ChunkSerializer.serialize(writer, chunk);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }
    
    @SneakyThrows
    public CompletableFuture<Void> restore(int chunkX, int chunkZ) {
        File archive = new File(this.archiveFolder, String.format(FILE_FORMAT, chunkX, chunkZ));
        if (!archive.exists()) {
            return CompletableFuture.completedFuture(null);
        }
        
        val reader = new BufferedInputStream(new FileInputStream(archive));
        val chunk = this.world.getChunkAt(chunkX, chunkZ);
        
        return CompletableFuture.supplyAsync(() -> {
            val deserializer = new ChunkDeserializer(reader, chunk);
            val task = RoninDistrict.getInstance().getServer().getScheduler().runTaskTimer(RoninDistrict.getInstance(), deserializer, 2L, 2L);
            while (true) {
                if (deserializer.isDone()) {
                    task.cancel();
                    break;
                }
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });
    }
    
    public boolean isArchived(int chunkX, int chunkZ) {
        File archive = new File(this.archiveFolder, String.format(FILE_FORMAT, chunkX, chunkZ));
        return archive.exists();
    }
}
