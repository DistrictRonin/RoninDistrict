package de.ronin.ronindistrict.world.archive;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.util.concurrent.atomic.AtomicBoolean;

@FieldDefaults(makeFinal = true)
public class ChunkDeserializer implements Runnable {

    private static final int BLOCKS_PER_TICK = 16 * 16;
    
    private Chunk chunk;
    private DataInputStream in;
    
    private AtomicBoolean pasteState;
    private BlockDataDictionary dictionary;
    
    @NonFinal private int x = 0, y = 0, z = 0;
    
    @Getter @NonFinal private boolean done;
    
    public ChunkDeserializer(BufferedInputStream reader, Chunk chunk) {
        this.chunk = chunk;
        this.in = new DataInputStream(reader);
        this.dictionary = new BlockDataDictionary();
        this.pasteState = new AtomicBoolean(false);
    }
    
    @Override
    @SneakyThrows
    public void run() {
        if (this.dictionary.getDictionary().size() <= 0) {
            val dictionarySize = this.in.readInt();
            for (int i = 0; i < dictionarySize; i++) {
                this.dictionary.add(
                        in.readInt(),
                        Bukkit.createBlockData(in.readUTF())
                );
            }
        }
        
        if (!this.pasteState.get()) {
            for (int i = 0; i < BLOCKS_PER_TICK * 2; i++) {
                this.chunk.getBlock(this.x, this.y, this.z).setType(Material.AIR, false);
    
                this.x++;
                if (this.x >= 16) {
                    this.x = 0;
                    this.z++;
                    if (this.z >= 16) {
                        this.z = 0;
                        this.y++;
                        if (this.y >= 256) {
                            this.x = 0;
                            this.y = 0;
                            this.z = 0;
                            this.pasteState.set(true);
                            return;
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < BLOCKS_PER_TICK; i++) {
                if(in.available() <= 0) {
                    this.done = true;
                    return;
                }
                val firstRead = this.in.readInt();
                if ((firstRead & ChunkSerializer.SET_BIT) != 0) {
                    this.x = firstRead & (~ChunkSerializer.SET_BIT);
                    this.y = in.readInt();
                    this.z = in.readInt();
        
                    this.chunk.getBlock(this.x, this.y, this.z).setBlockData(this.dictionary.lookup(in.readInt()), false);
                } else {
                    this.chunk.getBlock(this.x, this.y, this.z).setBlockData(this.dictionary.lookup(firstRead), false);
                }
                
                this.x++;
                if (this.x >= 16) {
                    this.x = 0;
                    this.z++;
                    if (this.z >= 16) {
                        this.z = 0;
                        this.y++;
                    }
                }
            }
        }
    }
}
