package de.ronin.ronindistrict.world.price;

import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@FieldDefaults(makeFinal = true)
public class LazyChunkPriceCache {

    private DistrictWorld world;
    
    private Map<ChunkPointer, Double> prices;
    
    public LazyChunkPriceCache(DistrictWorld world) {
        this.world = world;
        this.prices = Collections.synchronizedMap(new HashMap<>());
    }
    
    public CompletableFuture<Double> requestChunkPrice(ChunkPointer pointer) {
        if (this.prices.containsKey(pointer)) {
            return CompletableFuture.completedFuture(this.prices.get(pointer));
        } else {
            // Calculation may happen multiple times in a rare multi-threading scenario
            // However no data corruption appears so this is a very minor issue not worth bothering with
            return CompletableFuture.supplyAsync(() -> {
                val price = calculatePrice(pointer);
                this.prices.put(pointer, price);
                return price;
            });
        }
    }
    
    public void invalidateChunkPrices() {
        this.prices.clear();
    }
    
    private double calculatePrice(ChunkPointer pointer) {
        val holder = this.world.getSpawnChunkHolder();
        if (holder == null) {
            return 0.0;
        }
        if (this.world.getProtectionChunkHolder() == null) {
            return 0.0;
        }
        
        double distSquared = holder.values().stream()
                .flatMap(hol -> {
                    val stream = hol.getChunks().values().stream();
                    
                    val protection = world.getProtectionChunkHolder().get(hol.getId());
                    if (protection != null) {
                        return Stream.concat(stream, protection.getChunks().values().stream());
                    }
                    return stream;
                })
                .mapToDouble(po -> {
                    val dX = (double) po.getChunkX() - pointer.getChunkX();
                    val dZ = (double) po.getChunkZ() - pointer.getChunkZ();
                    return dX * dX + dZ * dZ;
                })
                .min().orElse(1.0);
        
        val dist = Math.sqrt(distSquared);
        return 1e6 * Math.pow(Math.E, -0.5 * dist);
    }
}
