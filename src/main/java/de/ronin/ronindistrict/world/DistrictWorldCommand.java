package de.ronin.ronindistrict.world;

import de.ostylk.baseapi.modules.command.Command;
import de.ronin.ronindistrict.RoninDistrict;
import lombok.experimental.FieldDefaults;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

@FieldDefaults(makeFinal = true)
public class DistrictWorldCommand {
    
    private DistrictWorldCache worldCache;
    
    public DistrictWorldCommand(RoninDistrict plugin) {
        this.worldCache = plugin.getWorldCache();
    }
    
    @Command("district.world.create")
    public void createWorld(CommandSender sender, World world) {
        this.worldCache.createWorld(world);
        this.worldCache.save();
        
        sender.sendMessage("\u00a7aDie Welt " + world.getName() + " wurde als Distrikt-Welt registriert.");
    }
    
    @Command("district.world.delete")
    public void deleteWorld(CommandSender sender, DistrictWorld world) {
        this.worldCache.deleteWorld(world.getUID());
        this.worldCache.save();
        
        sender.sendMessage("\u00a7cDie Welt " + world.getName() + " wird nicht mehr als Distrikt-Welt angesehen.");
    }
    
    @Command("district.world.list")
    public void listWorlds(CommandSender sender) {
        for (DistrictWorld world : this.worldCache.getWorlds().values()) {
            sender.sendMessage("\u00a76" + world.getName() + ": \u00a77" + world.getUID());
        }
    }
}
