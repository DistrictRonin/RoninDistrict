package de.ronin.ronindistrict.world;

import de.ostylk.baseapi.modules.config.Config;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true)
public class DistrictWorldCache {

    private Config backend;
    @Getter private Map<UUID, DistrictWorld> worlds;
    
    public DistrictWorldCache(Config backend) {
        this.backend = backend;
        this.worlds = new HashMap<>();
        
        this.backend.getSectionEntries("worlds").forEach(this::loadWorld);
    }
    
    private void loadWorld(String uuidStr) {
        UUID uuid = UUID.fromString(uuidStr);
        
        // TODO: load more important data
        
        this.worlds.put(uuid, new DistrictWorld(uuid));
    }
    
    public void createWorld(World world) {
        this.worlds.put(world.getUID(), new DistrictWorld(world.getUID()));
    }
    
    public void deleteWorld(UUID worldUUID) {
        this.worlds.remove(worldUUID);
    }
    
    public void save() {
        this.backend.set("worlds", null);
        this.worlds.forEach((uuid, world) -> {
            this.backend.set("worlds." + uuid.toString(), false);
            // TODO: save more important data
        });
        
        this.backend.save();
    }
    
    public boolean isManaged(World w) {
        return this.worlds.containsKey(w.getUID());
    }
    
    public CompletableFuture<Void> shutdown() {
        CompletableFuture[] futures = new CompletableFuture[this.worlds.size()];
        int i = 0;
        for (DistrictWorld world : this.worlds.values()) {
            futures[i] = world.getHolderIO().shutdown();
            i++;
        }
        
        return CompletableFuture.allOf(futures);
    }
}
