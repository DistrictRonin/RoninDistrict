package de.ronin.ronindistrict.world;

import de.ostylk.baseapi.modules.command.argument.ArgumentParser;
import de.ostylk.baseapi.modules.command.argument.ParsePayload;
import de.ronin.ronindistrict.RoninDistrict;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class DistrictWorldArgument implements ArgumentParser<DistrictWorld> {
    
    @Override
    public Set<String> getAllPossibilities() {
        return RoninDistrict.getInstance().getWorldCache().getWorlds().values().stream()
                .map(DistrictWorld::getName)
                .collect(Collectors.toSet());
    }
    
    @Override
    public ParsePayload<DistrictWorld> parse(CommandSender sender, String arg) {
        World world;
        if (arg.equals("$self")) {
            world = ((Player) sender).getWorld();
        } else {
            world = Bukkit.getWorld(arg);
        }
    
        if (world != null) {
            val district = RoninDistrict.getInstance().getWorldCache().getWorlds().get(world.getUID());
            if (district != null) {
                return ParsePayload.success(district);
            } else {
                return ParsePayload.error("Welt " + arg + " ist keine Distrikt-Welt.");
            }
        } else {
            return ParsePayload.error("Welt " + arg + " existiert nicht.");
        }
    }
    
    @Override
    public Class<DistrictWorld> getType() {
        return DistrictWorld.class;
    }
}
