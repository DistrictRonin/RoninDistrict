package de.ronin.ronindistrict.level;

import de.ronin.ronindistrict.plot.Plot;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.bukkit.Material;

import java.util.Map;

@FieldDefaults(makeFinal = true)
public class Level {

    @Getter private int level;
    @Getter private Map<Material, Integer> requirements;
    
    @Getter private double cost;
    
    public Level(int level, Map<Material, Integer> requirements, double cost) {
        this.level = level;
        this.requirements = requirements;
        this.cost = cost;
    }
    
    public LevelProgress createProgress(Plot plot) {
        return new LevelProgress(plot, this);
    }
}
