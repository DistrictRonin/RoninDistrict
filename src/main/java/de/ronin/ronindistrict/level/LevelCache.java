package de.ronin.ronindistrict.level;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import de.ostylk.baseapi.modules.config.Config;
import de.ronin.ronindistrict.world.DistrictWorld;
import de.ronin.ronindistrict.world.DistrictWorldCache;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import java.util.Objects;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class LevelCache {

    private Config backend;
    private DistrictWorldCache worldCache;
    
    @Getter private Table<DistrictWorld, Integer, Level> levels;
    
    public LevelCache(Config backend, DistrictWorldCache worldCache) {
        this.backend = backend;
        this.worldCache = worldCache;
        this.levels = HashBasedTable.create();
        this.load();
    }
    
    private void load() {
        this.backend.getSectionEntries("worlds").forEach(worldName -> {
            val world = Bukkit.getWorld(worldName);
            if (world == null) {
                System.err.println(worldName + " ignored for loading level requirements.");
                return;
            }
    
            val districtWorld = this.worldCache.getWorlds().get(world.getUID());
            if (districtWorld == null) {
                System.err.println(worldName + " ignored for loading level requirements");
                return;
            }
            
            this.loadWorld(districtWorld);
        });
    }

    private void loadWorld(DistrictWorld world) {
        this.backend.getSectionEntries("worlds." + world.getName()).forEach(levelStr -> {
            int level = Integer.parseInt(levelStr);
            
            val prefix = "worlds." + world.getName() + "." + levelStr + ".";
            double cost = this.backend.getDouble(prefix + "cost");
            val items = this.backend.getSectionEntries(prefix + "items").stream()
                    .map(materialStr -> {
                        val mat = Material.getMaterial(materialStr);
                        if (mat == null) {
                            System.err.println("Invalid material " + materialStr);
                            return null;
                        }
                        return materialStr;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toMap(Material::getMaterial,
                            mat -> backend.getInt(prefix + "items." + mat)));
            
            this.levels.put(world, level, new Level(level, items, cost));
        });
    }
}
