package de.ronin.ronindistrict.level;

import de.ronin.ronindistrict.plot.Plot;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@FieldDefaults(makeFinal = true)
public class LevelProgress {

    @Getter private Plot plot;
    @Getter private Level level;
    @Getter private Map<Material, Integer> delivered;
    @Getter @NonFinal private transient Map<Material, Integer> stillRequired = new HashMap<>();
    
    private DeliverWindow window;
    
    public LevelProgress(Plot plot, Level level) {
        this(plot, level, new HashMap<>());
    }
    
    public LevelProgress(Plot plot, Level level, Map<Material, Integer> delivered) {
        this.plot = plot;
        this.level = level;
        this.delivered = delivered;
        
        this.calculateStillRequired();
        this.window = new DeliverWindow(this);
    }
    
    public Map<Material, Integer> deliver(Map<Material, Integer> items) {
        val leftover = new HashMap<Material, Integer>();
        items.forEach((mat, count) -> {
            val newCount = this.delivered.getOrDefault(mat, 0) + count;
            val needed = this.level.getRequirements().get(mat);
            this.delivered.put(mat, Math.min(newCount, needed));
            
            if (newCount > needed) {
                leftover.put(mat, newCount - needed);
            }
        });
        
        this.calculateStillRequired();
        return leftover;
    }
    
    public boolean isFinished() {
        for (Material mat : this.delivered.keySet()) {
            if ((int) (this.delivered.get(mat)) < this.level.getRequirements().get(mat)) {
                return false;
            }
        }
        
        return this.level.getRequirements().size() == this.delivered.size();
    }
    
    public void finish() {
        if (!this.isFinished()) return;
        
        this.plot.setLevel(this.level.getLevel() + 1);
    }
    
    public double calculateProgress() {
        int max = 0;
        int current = 0;
        for (Material key : this.getLevel().getRequirements().keySet()) {
            max += this.getLevel().getRequirements().get(key);
            current += this.delivered.getOrDefault(key, 0);
        }
        
        return (double) (current) / max;
    }
    
    private void calculateStillRequired() {
        this.stillRequired.clear();
        for (Material key : this.level.getRequirements().keySet()) {
            val count = this.level.getRequirements().get(key);
            val left = count - this.delivered.getOrDefault(key, 0);
            if (left > 0) {
                this.stillRequired.put(key, left);
            }
        }
    }
    
    public void openWindow(Player p) {
        this.window.open(p);
    }
}
