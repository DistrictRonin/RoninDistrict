package de.ronin.ronindistrict.level;

import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.storage.FilterResult;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ronin.ronindistrict.RoninDistrict;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

@FieldDefaults(makeFinal = true)
public class DeliverWindow {

    private static final String ND_INFO = "nd_info";
    private static final String CONT_INFOBAR = "cont_infobar";
    
    private LevelProgress progress;
    private Map<Material, Integer> toDeliver;
    
    private Window<StorageContainer> root;
    
    public DeliverWindow(LevelProgress progress) {
        this.progress = progress;
        this.toDeliver = new HashMap<>();
        
        this.root = RoninDistrict.WINDOW.createStorageWindow(9, 5, false);
        this.root.getContainer().registerFilter((root, instance, type, event) -> {
            if (event.getItem().getItemMeta() instanceof Damageable) {
                if (((Damageable) event.getItem().getItemMeta()).hasDamage()) {
                    return FilterResult.BLOCK;
                }
            }

            if (progress.getStillRequired().containsKey(event.getItem().getType())) {
                return FilterResult.ALLOW;
            } else {
                return FilterResult.BLOCK;
            }
        });
        this.root.getContainer().registerInvalidationListener((root, instance) -> this.update(instance));
    
        this.root.registerCloseListener((root, instance, type) -> {
            if (type == Window.CloseType.POP) {
                val leftover = this.progress.deliver(this.toDeliver);
                this.toDeliver.clear();
    
                val inventory = instance.getPlayer().getInventory();
                leftover.forEach((mat, count) -> {
                    val stacks = count / mat.getMaxStackSize();
                    for (int i = 0; i < stacks; i++) {
                        inventory.addItem(new ItemStack(mat, mat.getMaxStackSize()));
                    }
                    
                    inventory.addItem(new ItemStack(mat, count - stacks * mat.getMaxStackSize()));
                });
            }
        });
        
        val infoBar = RoninDistrict.WINDOW.createNodeWindow(9, 1, false);
        infoBar.getContainer().nodeCreator().node(8, 0, this.infoNode(), ND_INFO);
        
        this.root.addChild(infoBar, 0, 4, CONT_INFOBAR);
        this.root.setChildMoveHandler("$root");
    }
    
    private void update(WindowInstance<StorageContainer> root) {
        this.toDeliver.clear();
        for (ItemStack is : root.getContainer().getContents()) {
            if (is == null) continue;
            if (is.getType() == Material.AIR) continue;
            
            val amount = this.toDeliver.getOrDefault(is.getType(), 0) + is.getAmount();
            this.toDeliver.put(is.getType(), amount);
        }
    
        val icon = this.infoNode();
        WindowInstance.WindowInstanceChild<NodeContainer> infoBar = root.searchChild(CONT_INFOBAR);
        infoBar.child.getContainer().getNode(ND_INFO).setIcon(icon);
        
        Window.WindowChild<NodeContainer> infoBarRoot = this.root.searchChild(CONT_INFOBAR);
        infoBarRoot.child.getContainer().getNode(ND_INFO).setIcon(icon);
    }
    
    private ItemStack infoNode() {
        val builder = RoninDistrict.ITEM.builder(Material.MINECART);
        final int[] counter = {0};
        this.progress.getStillRequired().forEach((mat, count) -> {
            val left = count - this.toDeliver.getOrDefault(mat, 0);
            if (left > 0) {
                builder.addLore("\u00a77" + left + "x " + StringUtils.capitalize(mat.name().toLowerCase().replace('_', ' ')));
                counter[0]++;
            }
        });
        
        if (counter[0] == 0) {
            return builder.name("\u00a7aFertig. Du kannst nun /plot claim nutzen").finish();
        } else {
            return builder.name("\u00a7cEs fehlt noch:").finish();
        }
    }
    
    public void open(Player p) {
        RoninDistrict.WINDOW.getContext(p).open(this.root, InventoryType.CHEST, "\u00a7aGrundstück aufleveln");
        RoninDistrict.WINDOW.getContext(p).triggerUpdate();
    }
}
