package de.ronin.ronindistrict.plot;

import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.level.LevelProgress;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import org.bukkit.Location;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@FieldDefaults(makeFinal = true)
public class Plot {

    @Getter private DistrictWorld world;
    @Getter private UUID owner;
    @Getter private Set<UUID> helpers;
    @Getter private Set<UUID> banned;
    
    @Getter @Setter @NonFinal private Location home;
    
    @Getter @NonFinal private LevelProgress progress;
    @Getter @NonFinal private int level;
    
    @Getter @Setter @NonFinal private int protectionExpansions;
    
    public Plot(DistrictWorld world, UUID owner, Location home) {
        this.world = world;
        this.owner = owner;
        this.helpers = new HashSet<>();
        this.banned = new HashSet<>();
        this.home = home;
        this.setLevel(1);
        this.protectionExpansions = 0;
    }
    
    public Plot(DistrictWorld world, UUID owner, Set<UUID> helpers, Set<UUID> banned, Location home, int level, int protectionExpansions) {
        this.world = world;
        this.owner = owner;
        this.helpers = helpers;
        this.banned = banned;
        this.home = home;
        this.level = level;
        this.protectionExpansions = protectionExpansions;
    }
    
    /** Only to be called by the deserializer */
    public void setProgress(LevelProgress progress) {
        this.progress = progress;
    }
    
    public void setLevel(int level) {
        this.level = level;
        val levelInst = RoninDistrict.getInstance().getLevelCache().getLevels().get(world, this.level);
        if (levelInst == null) {
            this.progress = null;
        } else {
            this.progress = levelInst.createProgress(this);
        }
    }
}
