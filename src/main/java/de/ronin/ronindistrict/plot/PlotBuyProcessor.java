package de.ronin.ronindistrict.plot;

import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.world.DistrictWorld;
import de.ronin.ronindistrict.world.price.LazyChunkPriceCache;
import lombok.experimental.FieldDefaults;
import lombok.val;
import lombok.var;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@FieldDefaults(makeFinal = true)
public class PlotBuyProcessor {

    public enum PlotBuyResult {
        SUCCESS,
        NOT_ENOUGH_MONEY,
        NOT_CONTINUOUS,
        NO_CHUNK_AVAILABLE,
        TOO_CLOSE_TO_OTHER,
        ALREADY_IN_USE,
    }
    
    @SettingLoad("plot.base_cost")
    public static double BASE_COST = 3000.0;
    
    private DistrictWorld world;
    private LazyChunkPriceCache chunkPriceCache;
    
    public PlotBuyProcessor(DistrictWorld world, LazyChunkPriceCache chunkPriceCache) {
        this.world = world;
        this.chunkPriceCache = chunkPriceCache;
    }
    
    private boolean isChunkInUse(Player p, ChunkPointer pointer) {
        val currentHolder = this.world.getHolders().get(pointer);
        if (currentHolder != null) {
            if (currentHolder instanceof ProtectionChunkHolder) {
                val protection = (ProtectionChunkHolder) currentHolder;
                val currentPlot = world.getPlotChunkHolder().get(p.getUniqueId());
                if (currentPlot == null || protection.getTarget() != currentPlot) {
                    return true;
                }
            } else {
                return true;
            }
        }
        
        return false;
    }
    
    private boolean tryCreateProtection(ChunkPointer center, PlotChunkHolder plot) {
        val RADIUS = 2;
        for (int dX = -RADIUS; dX <= RADIUS; dX++) {
            for (int dZ = -RADIUS; dZ <= RADIUS; dZ++) {
                val pointer = new ChunkPointer(
                        center.getChunkX() + dX,
                        center.getChunkZ() + dZ
                );
                if (this.world.getHolders().get(pointer) != null) {
                    return false;
                }
            }
        }
        
        val protection = new ProtectionChunkHolder<>(this.world, plot);
        this.world.getProtectionChunkHolder().add(protection);
        try {
            protection.setBox(center, RADIUS).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return true;
    }
    
    // TODO: this definetly needs to be cleaned up :)
    public CompletableFuture<PlotBuyResult> buy(Player p, ChunkPointer pointer) {
        if (this.isChunkInUse(p, pointer)) {
            return CompletableFuture.supplyAsync(() -> PlotBuyResult.ALREADY_IN_USE);
        }
    
        val costFuture = this.calculatePrice(p, pointer);
        return RoninDistrict.ECONOMY.getAccountAsync(p.getUniqueId(), RoninDistrict.ROLARI)
                .thenComposeAsync(account -> {
                    val cost = costFuture.join();
                    
                    if (account.getBalance() >= cost) {
                        var holder = world.getPlotChunkHolder().get(p.getUniqueId());
                        boolean newHolder = false;
                        if (holder == null) {
                            holder = new PlotChunkHolder(world, new Plot(this.world, p.getUniqueId(), p.getLocation().clone()));
                            if (!tryCreateProtection(pointer, holder)) {
                                return CompletableFuture.completedFuture(PlotBuyResult.TOO_CLOSE_TO_OTHER);
                            }
                            world.getPlotChunkHolder().add(holder);
                            newHolder = true;
                        } else {
                            if (holder.getPlot().getProgress() == null || !holder.getPlot().getProgress().isFinished()) {
                                if (newHolder) holder.destroy();
                                return CompletableFuture.completedFuture(PlotBuyResult.NO_CHUNK_AVAILABLE);
                            }
                        }
        
                        if (hasNeighboringChunk(holder, pointer)) {
                            account.withdraw(cost);
                            if (!newHolder) {
                                holder.getPlot().getProgress().finish();
                            }
                            return world.assignHolder(pointer, holder).thenApply(chunk -> PlotBuyResult.SUCCESS);
                        } else {
                            if (newHolder) holder.destroy();
                            return CompletableFuture.completedFuture(PlotBuyResult.NOT_CONTINUOUS);
                        }
                    } else {
                        return CompletableFuture.completedFuture(PlotBuyResult.NOT_ENOUGH_MONEY);
                    }
                });
    }
    
    public static boolean hasNeighboringChunk(PlotChunkHolder holder, ChunkPointer pointer) {
        if (holder.getChunks().size() == 0) return true;
        
        for (ChunkPointer other : holder.getChunks().values()) {
            val dX = Math.abs(other.getChunkX() - pointer.getChunkX());
            val dZ = Math.abs(other.getChunkZ() - pointer.getChunkZ());
            if (dX <= 1 && dZ <= 1 && (dX ^ dZ) == 1) {
                return true;
            }
        }
        
        return false;
    }
    
    public CompletableFuture<Double> calculatePrice(Player p, ChunkPointer pointer) {
        return this.chunkPriceCache.requestChunkPrice(pointer)
                .thenApply(distPrice -> {
                    val levelCost = this.calculateLevelPrice(p);
                    if (levelCost == Double.MAX_VALUE) {
                        return Double.MAX_VALUE;
                    }
                    
                    val cost = distPrice + BASE_COST + levelCost;
                    return (double) ((int) (cost / 5.0) * 5);
                });
    }
    
    private double calculateLevelPrice(Player p) {
        if (this.world.getPlotChunkHolder() == null) {
            return 0.0;
        }
        
        val plot = world.getPlotChunkHolder().get(p.getUniqueId());
        if (plot == null) {
            return 0.0;
        } else {
            if (plot.getPlot().getProgress() == null) {
                return Double.MAX_VALUE;
            } else {
                return plot.getPlot().getProgress().getLevel().getCost();
            }
        }
    }
}
