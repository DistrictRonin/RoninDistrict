package de.ronin.ronindistrict.plot.dynmap;


import lombok.Getter;

public class ChunkPointFace {
    
    @Getter
    private DirectionVertical vertical;
    @Getter
    private DirectionHorizontal horizontal;
    
    public ChunkPointFace(DirectionVertical vertical, DirectionHorizontal horizontal) {
        this.vertical = vertical;
        this.horizontal = horizontal;
    }
    
    public void merge(ChunkPointFace other) {
        this.vertical = this.vertical == other.vertical ?
                this.vertical : DirectionVertical.NONE;
        
        this.horizontal = this.horizontal == other.horizontal ?
                this.horizontal : DirectionHorizontal.NONE;
    }
}
