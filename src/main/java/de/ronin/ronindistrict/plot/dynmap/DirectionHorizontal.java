package de.ronin.ronindistrict.plot.dynmap;

import lombok.Getter;

public enum DirectionHorizontal {

    EAST(1),
    WEST(-1),
    NONE(0),
    ;
    
    @Getter
    private int offset;
    
    DirectionHorizontal(int offset) {
        this.offset = offset;
    }
}
