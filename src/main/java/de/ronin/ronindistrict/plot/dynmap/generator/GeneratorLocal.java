package de.ronin.ronindistrict.plot.dynmap.generator;

import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.plot.dynmap.ChunkPoint;
import de.ronin.ronindistrict.plot.dynmap.ChunkPointFace;
import de.ronin.ronindistrict.plot.dynmap.DirectionHorizontal;
import de.ronin.ronindistrict.plot.dynmap.DirectionVertical;
import lombok.val;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GeneratorLocal {

    public static List<ChunkPoint> generatePoints(Collection<ChunkPointer> chunks) {
        val points = new ArrayList<ChunkPoint>();
        for (ChunkPointer chunk : chunks) {
            createPoint(points, chunks, chunk, DirectionVertical.NORTH, DirectionHorizontal.EAST);
            createPoint(points, chunks, chunk, DirectionVertical.NORTH, DirectionHorizontal.WEST);
            createPoint(points, chunks, chunk, DirectionVertical.SOUTH, DirectionHorizontal.EAST);
            createPoint(points, chunks, chunk, DirectionVertical.SOUTH, DirectionHorizontal.WEST);
        }
    
        return points;
    }
    
    private static void createPoint(List<ChunkPoint> points,
                                    Collection<ChunkPointer> chunks,
                                    ChunkPointer source,
                                    DirectionVertical vertical,
                                    DirectionHorizontal horizontal) {
        val vd = new ChunkPointer(source.getChunkX() + vertical.getOffset(), source.getChunkZ());
        val hd = new ChunkPointer(source.getChunkX(), source.getChunkZ() + horizontal.getOffset());
        
        if (!chunks.contains(vd) || !chunks.contains(hd)) {
            val vOff = Math.max((vertical.getOffset() + 1) * 8 - 1, 0); // [-1;1]->[0;2]->[0;16]->[0;15]
            val hOff = Math.max((horizontal.getOffset() + 1) * 8 - 1, 0);
            points.add(new ChunkPoint(
                    new Vector(source.getChunkX() * 16 + hOff, 0, source.getChunkZ() * 16 + vOff),
                    new ChunkPointFace(vertical, horizontal),
                    source
            ));
        }
    }
}
