package de.ronin.ronindistrict.plot.dynmap;

import de.ronin.ronindistrict.holder.ChunkPointer;
import lombok.Getter;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class ChunkPoint {

    @Getter
    private Vector location;
    @Getter
    private ChunkPointFace face;
    @Getter
    private List<ChunkPointer> chunks;
    
    public ChunkPoint(Vector location, ChunkPointFace face, ChunkPointer chunk) {
        this.location = location;
        this.face = face;
        this.chunks = new ArrayList<>();
        this.chunks.add(chunk);
    }
    
    public void merge(ChunkPoint other) {
        // Create midpoint
        this.location.add(other.location).multiply(0.5);
        this.face.merge(other.face);
        this.chunks.addAll(other.chunks);
    }
}
