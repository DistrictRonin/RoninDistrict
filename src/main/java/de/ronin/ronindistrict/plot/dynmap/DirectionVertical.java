package de.ronin.ronindistrict.plot.dynmap;

import lombok.Getter;

public enum DirectionVertical {

    NORTH(-1),
    SOUTH(1),
    NONE(0),
    ;
    
    @Getter
    private int offset;
    
    DirectionVertical(int offset) {
        this.offset = offset;
    }
}
