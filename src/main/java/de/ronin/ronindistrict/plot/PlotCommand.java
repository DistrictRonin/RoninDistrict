package de.ronin.ronindistrict.plot;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.plot.command.IsMemberValidation.IsMember;
import de.ronin.ronindistrict.holder.plot.command.IsOwnerValidator.IsOwner;
import de.ronin.ronindistrict.util.PlayerNameUtil;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

@FieldDefaults(makeFinal = true)
public class PlotCommand {

    private PlotCommandCanceller canceller = new PlotCommandCanceller();
    private PlotCommandConfirmation confirmation = new PlotCommandConfirmation();
    
    @Command("plot.claim")
    public void plotClaim(Player player, @Optional("$self") DistrictWorld world) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
    
        val future = world.getCalculator().buy(player, pointer);
        future.thenAcceptAsync(result -> {
            switch (result) {
                case SUCCESS:
                    player.sendMessage("\u00a7aDu hast diesen Chunk gekauft.");
                    player.sendMessage("\u00a7a/plot info");
                    break;
                case NOT_CONTINUOUS:
                    player.sendMessage("\u00a7cDu darfst nur Chunks kaufen, die an deinem Grundstück angrenzen.");
                    break;
                case NOT_ENOUGH_MONEY:
                    player.sendMessage("\u00a7cDu hast nicht genug Geld, um den Chunk zu kaufen.");
                    break;
                case NO_CHUNK_AVAILABLE:
                    player.sendMessage("\u00a7cDu musst erst dein Grundstück aufleveln, bevor du einen neuen Chunk kaufen kannst.");
                    player.sendMessage("\u00a7c/plot level");
                    break;
                case TOO_CLOSE_TO_OTHER:
                    player.sendMessage("\u00a7cDu bist zu nah an einem bereits bewohnten Chunk dran.");
                    break;
                case ALREADY_IN_USE:
                    player.sendMessage("\u00a7cDu kannst diesen Chunk nicht kaufen.");
                    break;
            }
        });
    }
    
    private String showLevelProgress(Plot plot) {
        if (plot.getProgress() == null) {
            return "§7Du kannst keine weiteren Level und somit Chunks freischalten.";
        }
        val progress = (int) (plot.getProgress().calculateProgress() * 100);
    
        StringBuilder progressBar = new StringBuilder("§7[");
        for (int i = 0; i < 20; i++) {
            if (i < progress / (100 / 20)) {
                progressBar.append("§a|");
            } else {
                progressBar.append("§c|");
            }
        }
        
        progressBar.append("§7][§a").append(progress).append("%§7]");
        return progressBar.toString();
    }
    
    @Command("plot.info")
    public void plotInfo(Player player, @Optional("$self") PlotChunkHolder plot) {
        RoninDistrict.CONCURRENT.submitTask(() -> {
            val ownerName = PlayerNameUtil.requestName(plot.getPlot().getOwner());
            val helperUUIDs = new UUID[plot.getPlot().getHelpers().size()];
            val helperNames = PlayerNameUtil.requestName(plot.getPlot().getHelpers().toArray(helperUUIDs));
            val bannedUUIDs = new UUID[plot.getPlot().getBanned().size()];
            val bannedNames = PlayerNameUtil.requestName(plot.getPlot().getBanned().toArray(bannedUUIDs));
                
                
            player.sendMessage("§8╒═════ §6§l" + ownerName + "s Grundstück");
            player.sendMessage("§8╞§6Helfer: §7" + String.join(", ", helperNames));
            player.sendMessage("§8╞§6Hausverbote: §7" + String.join(", ", bannedNames));
            player.sendMessage("§8╞§6Level: §7" + plot.getPlot().getLevel());
            player.sendMessage("§8╞§6Level Fortschritt: " + this.showLevelProgress(plot.getPlot()));
            if (plot.getPlot().getProgress() != null) {
                player.sendMessage("§8╞§6/plot level");
            }
        });
    }
    
    @Command("plot.level")
    public void plotLevel(Player player, @IsMember @Optional("$self") PlotChunkHolder plot) {
        val ownerName = PlayerNameUtil.requestName(plot.getPlot().getOwner());
        player.sendMessage("§8╒═════ §6§l" + ownerName + "s Levelfortschritt");
        player.sendMessage("§8╞§6Level: §7" + plot.getPlot().getLevel());
        player.sendMessage("§8╞§6Level Fortschritt: " + this.showLevelProgress(plot.getPlot()));
        if (plot.getPlot().getProgress() == null) {
            return;
        }
        player.sendMessage("§8╞§6Noch benötigt: ");
        plot.getPlot().getProgress().getStillRequired().forEach((mat, count) -> {
            player.sendMessage("§8╞§7" + count + "x " + StringUtils.capitalize(mat.name().toLowerCase().replace('_', ' ')));
        });
        player.sendMessage("§8╞§6/plot level deliver §7um die Items abzugeben.");
    }
    
    @Command("plot.level.deliver")
    public void plotLevelDeliver(Player player, @IsMember @Optional("$self") PlotChunkHolder plot) {
        val progress = plot.getPlot().getProgress();
        if (progress == null) {
            player.sendMessage("\u00a7cEs gibt keine weiteren Grundstücks-Level");
        } else {
            progress.openWindow(player);
        }
    }
    
    @Command("plot.protection")
    public void plotProtection(Player player, @IsMember @Optional("$self") PlotChunkHolder plot) {
        val priceCount = PlotProtectionBuyProcessor.calculatePrice(plot);
        
        player.sendMessage("\u00a77Eine Schutzzonenerweiterung würde dich " + RoninDistrict.ROLARI.formatAmount(priceCount.getLeft()) + " kosten.");
        player.sendMessage("\u00a77Damit würdest du " + priceCount.getRight() + " Chunks für dich beanspruchen.");
        player.sendMessage("\u00a77Nutze \u00a76/plot protection buy \u00a77, um deine Schutzzone zu erweitern.");
    }
    
    @Command("plot.protection.buy")
    public void plotProtectionBuy(Player player, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        PlotProtectionBuyProcessor.buyProtection(player, plot);
    }
    
    @Command("plot.home")
    public void plotHome(Player player, @IsMember @Optional("$self") PlotChunkHolder plot) {
        player.sendMessage("\u00a7aTeleportation in 3s. Nicht bewegen...");
        canceller.getDelayedActions().put(player.getUniqueId(), Bukkit.getScheduler().runTaskLater(RoninDistrict.getInstance(), () -> {
            player.teleport(plot.getPlot().getHome());
        }, 20L * 3L));
    }
    
    @Command("plot.sethome")
    public void plotSetHome(Player player, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        if (!plot.getChunks().contains(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
            player.sendMessage("\u00a7cDu kannst dein Home nur innerhalb deines Grundstücks setzen.");
        } else {
            plot.getPlot().setHome(player.getLocation().clone());
            player.sendMessage("\u00a7aHome erfolgreich verschoben.");
        }
    }
    
    @Command("plot.addfriend")
    public void plotAddFriend(Player player, Player friend, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        if (!friend.getUniqueId().equals(plot.getPlot().getOwner())) {
            plot.getPlot().getHelpers().add(friend.getUniqueId());
            plot.update();
            player.sendMessage("\u00a7aFreund hinzugefügt.");
        } else {
            player.sendMessage("\u00a7cDu kannst dich nicht selber als Freund hinzufügen.");
        }
    }
    
    @Command("plot.removefriend")
    public void plotRemoveFriend(Player player, String friend, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        UUID[] uuids = new UUID[plot.getPlot().getHelpers().size()];
        plot.getPlot().getHelpers().toArray(uuids);
        val names = PlayerNameUtil.requestName(uuids);
        
        for (int i = 0; i < names.length; i++) {
            if (names[i].equalsIgnoreCase(friend)) {
                plot.getPlot().getHelpers().remove(uuids[i]);
                plot.update();
                player.sendMessage("\u00a7c" + friend + " wurde vom Grundstück entfernt.");
                return;
            }
        }
        player.sendMessage("\u00a7cDieser Spieler ist nicht auf deinem Grundstück hinzugefügt.");
    }
    
    @Command("plot.ban")
    public void plotBan(Player player, Player ban, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        if (!ban.getUniqueId().equals(plot.getPlot().getOwner())) {
            plot.getPlot().getBanned().add(ban.getUniqueId());
            player.sendMessage("\u00a7cHausverbot erteilt.");
        } else {
            player.sendMessage("\u00a7cDu kannst dir nicht selber Hausverbot erteilen.");
        }
    }
    
    @Command("plot.unban")
    public void plotUnban(Player player, String unban, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        UUID[] uuids = new UUID[plot.getPlot().getBanned().size()];
        plot.getPlot().getBanned().toArray(uuids);
        val names = PlayerNameUtil.requestName(uuids);
        for (int i = 0; i < names.length; i++) {
            if (names[i].equalsIgnoreCase(unban)) {
                plot.getPlot().getBanned().remove(uuids[i]);
                player.sendMessage("\u00a7a" + unban + " darf wieder auf dein Grundstück.");
                return;
            }
        }
        player.sendMessage("\u00a7cDieser Spieler hat kein Hausverbot.");
    }
    
    @Command("plot.accept")
    public void plotAccept(Player player) {
        this.confirmation.accept(player);
    }
    
    @Command("plot.decline")
    public void plotDecline(Player player) {
        this.confirmation.decline(player);
    }
    
    @Command("plot.delete")
    public void plotDelete(Player player, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
        if (!plot.getWorld().getUID().equals(player.getLocation().getWorld().getUID())
                || !plot.getChunks().contains(pointer.getChunkX(), pointer.getChunkZ())) {
            player.sendMessage("\u00a7cDu befindest dich auf keinem Chunk, dass gelöscht werden könnte.");
            return;
        }
        
        if (plot.getChunks().size() <= 1) {
            player.sendMessage("\u00a7cDas ist dein letzter Chunk. Wenn du dein gesamten Plot löschen willst, nutze bitte /plot delete all");
            return;
        }
        
        player.sendMessage("\u00a76Willst du wirklich diesen Chunk von deinem GS löschen?");
        player.sendMessage("\u00a7a\u00a7l/plot accept \u00a77oder \u00a7c\u00a7l/plot decline");
        this.confirmation.add(player, () -> player.sendMessage("\u00a7cAbgebrochen"), () -> {
            player.sendMessage("\u00a7cChunk wird nun gelöscht und ist für den Reset eingereiht.");
            plot.getWorld().queueDeassignment(pointer);
        });
    }
    
    @Command("plot.delete.all")
    public void plotDeleteAll(Player player, @IsOwner @Optional("$self") PlotChunkHolder plot) {
        player.sendMessage("\u00a76Willst du wirklich dein GESAMTES Grundstück löschen?");
        player.sendMessage("\u00a7a\u00a7l/plot accept \u00a77oder \u00a7c\u00a7l/plot decline");
        this.confirmation.add(player, () -> player.sendMessage("\u00a7cAbgebrochen"), () -> {
            plot.getWorld().getPlotChunkHolder().remove(player.getUniqueId());
            val values = new HashSet<>(plot.getChunks().values());
            for (ChunkPointer pointer : values) {
                plot.getWorld().queueDeassignment(pointer);
            }
            plot.destroy();
            player.sendMessage("\u00a7cGrundstück gelöscht. Alle Chunks wurden zum Reset eingereiht.");
        });
    }
}
