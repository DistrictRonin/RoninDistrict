package de.ronin.ronindistrict.plot;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.protection.ChunkHolderProtectible;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.util.PlayerArmorstand;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

import java.lang.ref.WeakReference;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true)
public class PlotCostHUD implements Listener {

    private DistrictWorld world;
    private PlotBuyProcessor calculator;
    
    private Multimap<UUID, PlayerArmorstand> huds;
    
    public PlotCostHUD(DistrictWorld world, PlotBuyProcessor calculator) {
        this.world = world;
        this.calculator = calculator;
        this.huds = MultimapBuilder.hashKeys().hashSetValues().build();
    
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.getLocation().getWorld().getUID().equals(this.world.getUID())) {
                this.update(player, player.getLocation().getChunk());
            }
        });
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (e.getPlayer().getLocation().getWorld().getUID().equals(this.world.getUID())) {
            this.update(e.getPlayer(), e.getPlayer().getLocation().getChunk());
        }
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.huds.removeAll(e.getPlayer().getUniqueId()).forEach(PlayerArmorstand::remove);
    }
    
    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent e) {
        if (e.getFrom().getUID().equals(this.world.getUID())) {
            this.huds.removeAll(e.getPlayer().getUniqueId()).forEach(PlayerArmorstand::remove);
        }
        if (e.getPlayer().getLocation().getWorld().getUID().equals(this.world.getUID())) {
            this.update(e.getPlayer(), e.getPlayer().getLocation().getChunk());
        }
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getFrom().getWorld().getUID().equals(this.world.getUID())) return;
        val previous = e.getFrom().getChunk();
        if (e.getTo() == null) return;
        val next = e.getTo().getChunk();
        if (previous != next) {
            this.update(e.getPlayer(), next);
        }
    }
    
    @EventHandler
    public void onTeleport(PlayerTeleportEvent e) {
        if (e.getTo() == null) return;
        if (!e.getTo().getWorld().getUID().equals(this.world.getUID())) return;
        
        this.update(e.getPlayer(), e.getTo().getChunk());
    }
    
    private void update(Player viewer, Chunk source) {
        this.huds.removeAll(viewer.getUniqueId()).forEach(PlayerArmorstand::remove);
        
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() - 1, source.getZ() - 1));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() - 1, source.getZ()));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() - 1, source.getZ() + 1));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX(), source.getZ() - 1));
        this.updateArmorstand(viewer, source);
        this.updateArmorstand(viewer, world.getChunkAt(source.getX(), source.getZ() + 1));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() + 1, source.getZ() - 1));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() + 1, source.getZ()));
        this.updateArmorstand(viewer, world.getChunkAt(source.getX() + 1, source.getZ() + 1));
    }
    
    private void updateArmorstand(Player viewer, Chunk chunk) {
        val pa = new PlayerArmorstand(viewer, this.locationFromChunk(chunk));
        pa.setInvisible(true);
        this.huds.put(viewer.getUniqueId(), pa);
    
        val weakRef = new WeakReference<PlayerArmorstand>(pa);
        this.nameFromChunk(viewer, chunk)
                .thenAccept(display -> {
                    val armorstand = weakRef.get();
                    if (armorstand != null) {
                        if (!display.trim().isEmpty()) {
                            armorstand.setName(display);
                        } else {
                            armorstand.remove();
                        }
                    }
                });
    }
    
    private CompletableFuture<String> showPrice(Player p, Chunk chunk, boolean tainted) {
        val plot = this.world.getPlotChunkHolder().get(p.getUniqueId());
        if (plot != null && !PlotBuyProcessor.hasNeighboringChunk(plot, new ChunkPointer(chunk.getX(), chunk.getZ()))) {
            return CompletableFuture.completedFuture("");
        }
        return this.calculator.calculatePrice(p, new ChunkPointer(chunk.getX(), chunk.getZ()))
                .thenApply(price -> {
                    String tag;
                    if (price == Double.MAX_VALUE) {
                        if (!tainted) {
                            tag = "\u00a7cKosten: \u00a77/";
                        } else {
                            tag = "\u00a74Kosten: \u00a77/";
                        }
                    } else {
                        if (!tainted) {
                            tag = "\u00a7aKosten: \u00a77" + RoninDistrict.ROLARI.formatAmount(price);
                        } else {
                            tag = "\u00a75Kosten: \u00a77" + RoninDistrict.ROLARI.formatAmount(price);
                        }
                    }
                    
                    return tag;
                });
        
    }
    
    private CompletableFuture<String> nameFromChunk(Player p, Chunk chunk) {
        val holder = this.world.getHolders().get(new ChunkPointer(chunk.getX(), chunk.getZ()));
        if (holder == null) {
            return showPrice(p, chunk, false);
        } else if (holder.hudDisplay().isPresent()) {
            if (holder instanceof ProtectionChunkHolder<?>) {
                if (((ChunkHolderProtectible) ((ProtectionChunkHolder) holder).getTarget()).isOwner(p.getUniqueId())) {
                    return showPrice(p, chunk, true)
                            .thenApply(str -> {
                                if (str.isEmpty()) return holder.hudDisplay().get();
                                return str;
                            });
                }
            }
            
            return CompletableFuture.completedFuture(holder.hudDisplay().get());
        }
        
        return CompletableFuture.completedFuture("");
    }
    
    private Location locationFromChunk(Chunk chunk) {
        val x = chunk.getX() * 16 + 8;
        val z = chunk.getZ() * 16 + 8;
        return new Location(
                chunk.getWorld(),
                x,
                chunk.getWorld().getHighestBlockYAt(x, z) + 1,
                z
        );
    }
}
