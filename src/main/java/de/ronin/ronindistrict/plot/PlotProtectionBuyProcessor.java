package de.ronin.ronindistrict.plot;

import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import lombok.val;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.entity.Player;

public class PlotProtectionBuyProcessor {

    @SettingLoad("plot.protection_base_cost")
    public static double BASE_COST = 500.0;
    
    public static void buyProtection(Player owner, PlotChunkHolder plot) {
        val price = calculatePrice(plot).getLeft();
        ProtectionChunkHolder<?> protection = plot.getWorld().getProtectionChunkHolder().get(plot.getId());
        val newChunks = protection.radiateOutwards();
        
        RoninDistrict.ECONOMY.getAccountAsync(owner.getUniqueId(), RoninDistrict.ROLARI)
                .whenCompleteAsync((account, throwable) -> {
                    switch (account.withdraw(price)) {
                        case SUCCESS:
                            for (ChunkPointer pointer : newChunks) {
                                plot.getWorld().assignHolder(pointer, protection);
                            }
                            plot.getPlot().setProtectionExpansions(plot.getPlot().getProtectionExpansions() + 1);
                            owner.sendMessage("\u00a7aSchutzzonenerweiterung erfolgreich gekauft.");
                            break;
                        case NOT_ENOUGH_MONEY:
                            owner.sendMessage("\u00a7cDu hast nicht genug Geld diese Schutzzonenerweiterung zu kaufen.");
                            break;
                    }
                });
    }
    
    public static Pair<Double, Integer> calculatePrice(PlotChunkHolder plot) {
        val protection = plot.getWorld().getProtectionChunkHolder().get(plot.getId());
        val newChunks = protection.radiateOutwards();
        
        return new ImmutablePair<>(
                BASE_COST * newChunks.size() * Math.pow(plot.getPlot().getProtectionExpansions() + 1, 2),
                newChunks.size()
        );
    }
}
