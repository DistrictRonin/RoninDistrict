package de.ronin.ronindistrict.plot;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import de.ronin.ronindistrict.RoninDistrict;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.UUID;

@FieldDefaults(makeFinal = true)
public class PlotCommandCanceller implements Listener, Runnable {
    
    @Getter private Multimap<UUID, BukkitTask> delayedActions;
    
    public PlotCommandCanceller() {
        this.delayedActions = MultimapBuilder.hashKeys().linkedListValues().build();
        Bukkit.getPluginManager().registerEvents(this, RoninDistrict.getInstance());
        Bukkit.getScheduler().runTaskTimer(RoninDistrict.getInstance(), this, 20L * 10L, 20L * 10L);
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo() == null) return;
        val dist = e.getFrom().toVector().distanceSquared(e.getTo().toVector());
        if (dist >= 0.01) {
            val tasks = delayedActions.removeAll(e.getPlayer().getUniqueId());
            for (BukkitTask task : tasks) {
                task.cancel();
            }
        }
    }
    
    @Override
    public void run() {
        val iterator = this.delayedActions.values().iterator();
        val pendingTasks = Bukkit.getScheduler().getPendingTasks();
        while (iterator.hasNext()) {
            if(!pendingTasks.contains(iterator.next())) {
                iterator.remove();
            }
        }
    }
}
