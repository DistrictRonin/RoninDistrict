package de.ronin.ronindistrict.plot;

import lombok.experimental.FieldDefaults;
import lombok.val;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@FieldDefaults(makeFinal = true)
public class PlotCommandConfirmation {

    // confirmation requests are invalid after 1 minute
    private static long MAX_THRESHOLD = 1000L * 60L;
    
    private Map<UUID, Triple<Long, Runnable, Runnable>> confirmation;
    
    public PlotCommandConfirmation() {
        this.confirmation = new HashMap<>();
    }
    
    public void add(Player player, Runnable decline, Runnable accept) {
        this.confirmation.put(player.getUniqueId(), new ImmutableTriple<>(System.currentTimeMillis() + MAX_THRESHOLD, decline, accept));
    }
    
    public void decline(Player player) {
        val pair = this.confirmation.remove(player.getUniqueId());
        if (pair != null && pair.getLeft() > System.currentTimeMillis()) {
            pair.getMiddle().run();
        }
    }
    
    public void accept(Player player) {
        val pair = this.confirmation.remove(player.getUniqueId());
        if (pair != null && pair.getLeft() > System.currentTimeMillis()) {
            pair.getRight().run();
        }
    }
}
