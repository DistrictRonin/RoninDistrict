package de.ronin.ronindistrict.util;

import lombok.val;
import net.minecraft.server.v1_14_R1.Entity;

import java.util.concurrent.atomic.AtomicInteger;

public class ReflectionHelper {
    
    private ReflectionHelper() {}
    
    public static Object getPrivateField(final Class<?> clazz, final String field, final Object object) {
        try {
            val result = clazz.getDeclaredField(field);
            result.setAccessible(true);
            return result.get(object);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalArgumentException("Failed to get field", e);
        }
    }
    
    public static void setPrivateField(final Class<?> clazz, final String field, final Object object,
                                       final Object value) {
        try {
            val result = clazz.getDeclaredField(field);
            result.setAccessible(true);
            result.set(object, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalArgumentException("Failed to set field", e);
        }
    }
    
    public static int getEntityID() {
        return ((AtomicInteger) getPrivateField(Entity.class, "entityCount", null)).incrementAndGet();
    }
}
