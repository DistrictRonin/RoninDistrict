package de.ronin.ronindistrict.util;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerEntityTeleport;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import net.minecraft.server.v1_14_R1.EntityTypes;
import net.minecraft.server.v1_14_R1.PacketPlayOutSpawnEntity;
import net.minecraft.server.v1_14_R1.Vec3D;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@FieldDefaults(makeFinal = true)
public class PlayerArmorstand {
    
    @Getter private Player viewer;
    
    private int entityId;
    @NonFinal private byte status;
    @NonFinal @Getter private Location location;
    
    @NonFinal @Getter private String name;
    
    private Unsafe unsafe = new Unsafe();
    
    public PlayerArmorstand(Player viewer, Location location) {
        this.viewer = viewer;
        this.entityId = ReflectionHelper.getEntityID();
        this.location = location;
        
        this.unsafe.spawnArmorStand();
    }
    
    public void setName(String name) {
        if (name == null) name = "";
        this.name = name;
        
        this.unsafe.setName(name);
    }
    
    public void setInvisible(boolean invisible) {
        if (invisible) {
            status |= (byte) 0x20;
        } else {
            status &= (byte) ~0x20;
        }
        
        this.unsafe.setStatusByte();
    }
    
    public void move(Location location) {
        this.location = location;
        this.unsafe.teleport();
    }
    
    public void remove() {
        this.unsafe.destroy();
    }
    
    private class Unsafe {
        private void spawnArmorStand() {
            val packet = new PacketPlayOutSpawnEntity(
                    entityId,
                    UUID.randomUUID(),
                    location.getX(),
                    location.getY(),
                    location.getZ(),
                    location.getPitch(),
                    location.getYaw(),
                    EntityTypes.ARMOR_STAND,
                    0,
                    new Vec3D(0D, 0D, 0D)
            );
    
            ((CraftPlayer) viewer).getHandle().playerConnection.sendPacket(packet);
        }
        
        private void setName(String name) {
            val opt = Optional.of(WrappedChatComponent.fromChatMessage(name)[0].getHandle());
            val wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.setEntityID(entityId);
            wrapper.setMetadata(Arrays.asList(
                    new WrappedWatchableObject(new WrappedDataWatcherObject(2, Registry.getChatComponentSerializer(true)), opt),
                    new WrappedWatchableObject(new WrappedDataWatcherObject(3, Registry.get(Boolean.class)), !name.isEmpty())
            ));
            wrapper.sendPacket(viewer);
        }
        
        private void setStatusByte() {
            val wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.setEntityID(entityId);
            wrapper.setMetadata(Collections.singletonList(
                    new WrappedWatchableObject(new WrappedDataWatcherObject(0, Registry.get(Byte.class)), status)
            ));
            wrapper.sendPacket(viewer);
        }
        
        private void teleport() {
            val wrapper = new WrapperPlayServerEntityTeleport();
            wrapper.setEntityID(entityId);
            wrapper.setX(location.getX());
            wrapper.setY(location.getY());
            wrapper.setZ(location.getZ());
            wrapper.setPitch(location.getPitch());
            wrapper.setYaw(location.getYaw());
            wrapper.sendPacket(viewer);
        }
        
        private void destroy() {
            val wrapper = new WrapperPlayServerEntityDestroy();
            wrapper.setEntityIds(new int[] { entityId });
            wrapper.sendPacket(viewer);
        }
    }
}
