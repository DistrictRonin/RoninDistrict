package de.ronin.ronindistrict.util;

import de.ronin.ronindistrict.RoninDistrict;
import lombok.val;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PlayerNameUtil {

    public static String requestName(UUID uuid) {
        try {
            return RoninDistrict.PLAYER_CACHE.requestPlayerNameAsync(uuid).get(20, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            return "Spieler";
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String[] requestName(UUID[] uuids) {
        try {
            return RoninDistrict.PLAYER_CACHE.requestPlayerNameAsync(uuids).get(40, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            val names = new String[uuids.length];
            for (int i = 0; i < uuids.length; i++) {
                names[i] = "Spieler";
            }
            return names;
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
