package de.ronin.ronindistrict.holder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

public interface ChunkHolderDeserializer<T extends ChunkHolder> {

    T read(File file, DistrictWorld world);
    
    @SneakyThrows
    default void readChunks(JsonArray chunks, DistrictWorld world, T holder) {
        val futures = new ArrayList<CompletableFuture<Void>>(chunks.size());
        for (JsonElement chunk : chunks) {
            val array = chunk.getAsJsonArray();
        
            val pointer = new ChunkPointer(array.get(0).getAsInt(), array.get(1).getAsInt());
            futures.add(world.assignHolder(pointer, holder));
        }
        val ite = futures.iterator();
        while (ite.hasNext()) {
            ite.next().get();
        }
        futures.clear();
    }
}
