package de.ronin.ronindistrict.holder.server;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.entity.Player;

@FieldDefaults(makeFinal = true)
public class ServerChunkHolderCommand {

    @Command("district.chunk.server.make")
    public void chunkServerMake(Player player, @Optional("$self") DistrictWorld world) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
    
        val future = world.assignHolder(pointer, world.getServerChunkHolder());
        player.sendMessage("\u00a7aThis chunk is being transformed to a server chunk.");
        future.whenCompleteAsync((chunk, throwable) -> {
            player.sendMessage("\u00a7aDone.");
        });
    }
}
