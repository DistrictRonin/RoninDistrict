package de.ronin.ronindistrict.holder.server;

import com.google.gson.Gson;
import de.ronin.ronindistrict.holder.ChunkHolderSerializer;
import lombok.val;

import java.io.File;

public class ServerChunkHolderSerializer implements ChunkHolderSerializer<ServerChunkHolder> {

    private File file;
    private ServerChunkHolder holder;
    
    private final Gson gson = new Gson();
    
    @Override
    public void init(File file, ServerChunkHolder holder) {
        this.file = file;
        this.holder = holder;
    }
    
    @Override
    public void write() {
        val chunks = this.writeChunks(this.holder);
        this.writeFile(this.gson, this.file, chunks);
    }
}
