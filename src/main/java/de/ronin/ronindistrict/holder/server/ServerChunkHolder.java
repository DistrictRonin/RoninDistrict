package de.ronin.ronindistrict.holder.server;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class ServerChunkHolder extends ChunkHolder {
    
    public static final String ID = "ServerChunk";
    private static final String MODIFY_PERMISSION = "ronindistrict.chunk.server.modify";
    
    private Map<ChunkPointer, ProtectedRegion> regions;
    
    public ServerChunkHolder(DistrictWorld world) {
        super(ID, world, new ServerChunkHolderSerializer());
        this.regions = new HashMap<>();
    }
    
    @Override
    protected void onAdd(ChunkPointer pointer) {
        val x = pointer.getChunkX() * 16;
        val z = pointer.getChunkZ() * 16;
        
        val region = new ProtectedCuboidRegion("server_" + pointer.getChunkX() + "_" + pointer.getChunkZ(), true,
                BlockVector3.at(x, 0, z), BlockVector3.at(x + 15, 255, z + 15));
        
        this.getWorld().getRegionManager().addRegion(region);
        this.regions.put(pointer, region);
    }
    
    @Override
    protected void onRemove(ChunkPointer pointer) {
        String id = "server_" + pointer.getChunkX() + "_" + pointer.getChunkZ();
        this.regions.remove(pointer);
        this.getWorld().getRegionManager().removeRegion(id);
    }
    
    @Override
    protected List<String> holderInfo() {
        val info = new ArrayList<String>();
        info.add("\u00a77---- \u00a76Server Chunk \u00a77----");
        info.add("\u00a77Chunks: " + String.join(", ",
                this.getChunks().values().stream()
                        .map(pointer -> pointer.getChunkX() + "|" + pointer.getChunkZ())
                        .collect(Collectors.toSet())
        ));
        return info;
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (e.getPlayer().hasPermission(MODIFY_PERMISSION)) {
            RoninDistrict.CONCURRENT.submitTask(() -> {
                this.regions.values().forEach(region -> region.getMembers().addPlayer(e.getPlayer().getUniqueId()));
            });
        }
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        RoninDistrict.CONCURRENT.submitTask(() -> {
            this.regions.values().forEach(region -> region.getMembers().removePlayer(e.getPlayer().getUniqueId()));
        });
    }
}
