package de.ronin.ronindistrict.holder.server;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import de.ronin.ronindistrict.holder.ChunkHolderDeserializer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.io.FileReader;

public class ServerChunkHolderDeserializer implements ChunkHolderDeserializer<ServerChunkHolder> {
    
    private final Gson gson = new Gson();
    
    @Override
    @SneakyThrows
    public ServerChunkHolder read(File file, DistrictWorld world) {
        val holder = new ServerChunkHolder(world);
        
        val chunks = gson.fromJson(new FileReader(file), JsonArray.class);
        this.readChunks(chunks, world, holder);
        return holder;
    }
}
