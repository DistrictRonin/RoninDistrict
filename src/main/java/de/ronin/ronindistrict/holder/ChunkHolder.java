package de.ronin.ronindistrict.holder;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true)
public abstract class ChunkHolder implements Listener {
    
    @Getter private String id;
    @Getter private DistrictWorld world;
    @Getter private Table<Integer, Integer, ChunkPointer> chunks;
    private ChunkHolderSerializer serializer;
    
    public ChunkHolder(String id, DistrictWorld world, ChunkHolderSerializer serializer) {
        this.id = id;
        this.world = world;
        this.chunks = HashBasedTable.create();
        this.serializer = serializer;
        
        //noinspection unchecked
        this.serializer.init(
                new File(world.getHolderIO().getHolderFolder(), id),
                this
        );
        Bukkit.getPluginManager().registerEvents(this, RoninDistrict.getInstance());
    }
    
    public void registerChunk(ChunkPointer pointer) {
        synchronized (this.chunks) {
            this.chunks.put(pointer.getChunkX(), pointer.getChunkZ(), pointer);
            this.onAdd(pointer);
        }
    }
    
    public void unregisterChunk(ChunkPointer pointer) {
        synchronized (this.chunks) {
            this.onRemove(pointer);
            this.chunks.remove(pointer.getChunkX(), pointer.getChunkZ());
        }
    }
    
    public CompletableFuture<Void> serialize() {
        return CompletableFuture.runAsync(this.serializer::write);
    }
    
    public void destroy() {
        HandlerList.unregisterAll(this);
    }
    
    public Optional<String> hudDisplay() {
        return Optional.empty();
    }
    
    protected abstract void onAdd(ChunkPointer pointer);
    protected abstract void onRemove(ChunkPointer pointer);
    protected abstract List<String> holderInfo();
}
