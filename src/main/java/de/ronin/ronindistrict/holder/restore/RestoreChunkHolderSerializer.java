package de.ronin.ronindistrict.holder.restore;

import com.google.gson.Gson;
import de.ronin.ronindistrict.holder.ChunkHolderSerializer;
import lombok.val;

import java.io.File;

public class RestoreChunkHolderSerializer implements ChunkHolderSerializer<RestoreChunkHolder> {

    private File file;
    private RestoreChunkHolder holder;
    
    private final Gson gson = new Gson();
    
    @Override
    public void init(File file, RestoreChunkHolder holder) {
        this.file = file;
        this.holder = holder;
    }
    
    @Override
    public void write() {
        val chunks = this.writeChunks(this.holder);
        this.writeFile(this.gson, this.file, chunks);
    }
}
