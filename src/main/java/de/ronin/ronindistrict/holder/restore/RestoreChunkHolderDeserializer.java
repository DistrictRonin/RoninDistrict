package de.ronin.ronindistrict.holder.restore;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import de.ronin.ronindistrict.holder.ChunkHolderDeserializer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.io.FileReader;

public class RestoreChunkHolderDeserializer implements ChunkHolderDeserializer<RestoreChunkHolder> {
    
    private final Gson gson = new Gson();
    
    @Override
    @SneakyThrows
    public RestoreChunkHolder read(File file, DistrictWorld world) {
        val holder = new RestoreChunkHolder(world);
    
        val chunks = gson.fromJson(new FileReader(file), JsonArray.class);
        this.readChunks(chunks, world, holder);
        return holder;
    }
}
