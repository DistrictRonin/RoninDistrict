package de.ronin.ronindistrict.holder.restore;

import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class RestoreChunkHolder extends ChunkHolder implements Runnable {
    
    public static final String ID = "RestoreChunk";
    private static final long INTERVAL = 20L * 60L; // 1 minute
    
    public RestoreChunkHolder(DistrictWorld world) {
        super(ID, world, new RestoreChunkHolderSerializer());
        Bukkit.getScheduler().runTaskTimerAsynchronously(RoninDistrict.getInstance(), this, INTERVAL, INTERVAL);
    }
    
    @Override
    protected void onAdd(ChunkPointer pointer) { }
    
    @Override
    protected void onRemove(ChunkPointer pointer) { }
    
    @Override
    protected List<String> holderInfo() {
        val info = new ArrayList<String>();
        info.add("\u00a77---- \u00a76Restore Chunk \u00a77----");
        info.add("\u00a77Chunks: " + String.join(", ",
                this.getChunks().values().stream()
                        .map(pointer -> pointer.getChunkX() + "|" + pointer.getChunkZ())
                        .collect(Collectors.toSet())));
        return info;
    }
    
    private boolean isEncased(int chunkX, int chunkZ) {
        return this.getChunks().contains(chunkX + 1, chunkZ) &&
                this.getChunks().contains(chunkX - 1, chunkZ) &&
                this.getChunks().contains(chunkX, chunkZ + 1) &&
                this.getChunks().contains(chunkX, chunkZ - 1);
    }
    
    private ChunkPointer searchNorthBorderChunk(int chunkX, int chunkZ) {
        if (this.getChunks().contains(chunkX, chunkZ - 1)) {
            return this.searchNorthBorderChunk(chunkX, chunkZ - 1);
        } else {
            return new ChunkPointer(chunkX, chunkZ);
        }
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getPlayer().getLocation().getWorld().getUID().equals(this.getWorld().getUID())) {
            return;
        }
        
        val chunkX = e.getTo().getChunk().getX();
        val chunkZ = e.getTo().getChunk().getZ();
        if (this.getChunks().contains(chunkX, chunkZ)) {
            // Prevent the player from being stuck and thrown around indefinitely
            if (this.isEncased(chunkX, chunkZ)) {
                val northFreeChunk = this.searchNorthBorderChunk(chunkX, chunkZ);
                val inChunkBlock = this.getWorld().getChunkAt(northFreeChunk).getBlock(8, 0, 0);
                val blockToTeleport = this.getWorld().getHighestBlockAt(inChunkBlock.getLocation().add(0, 0, -1));
                e.getPlayer().teleport(blockToTeleport.getLocation().add(0, 2, 0));
                return;
            }
            
            Vector pos = e.getPlayer().getLocation().toVector();
            Vector toMid = new Vector(chunkX * 16 + 7.5, pos.getY(), chunkZ * 16 + 7.5).subtract(pos);
            Vector fromMid = toMid.normalize().multiply(-1);
            e.getPlayer().setVelocity(fromMid.add(new Vector(0, 0.1, 0)).multiply(2f));
            e.getPlayer().sendMessage("\u00a7cDieser Chunk wird gerade resettet.");
        }
    }
    
    @Override
    public void run() {
        ChunkPointer pointer = null;
        synchronized (this.getChunks()) {
            val iterator = this.getChunks().cellSet().iterator();
            if (iterator.hasNext()) {
                pointer = iterator.next().getValue();
            }
        }
    
        if (pointer != null) {
            this.getWorld().deassignHolder(pointer);
        }
    }
}
