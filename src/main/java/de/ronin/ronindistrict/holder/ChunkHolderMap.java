package de.ronin.ronindistrict.holder;

import lombok.SneakyThrows;
import lombok.val;

import java.io.FilenameFilter;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ChunkHolderMap<K, V extends ChunkHolder> {

    private Map<K, V> holders;
    private ChunkHolderIO holderIO;
    private Function<V, K> key;
    
    @SneakyThrows
    public ChunkHolderMap(ChunkHolderIO holderIO, Class<V> type, FilenameFilter filter, Function<V, K> key) {
        this.holders = holderIO.load(type, filter).get().stream()
                .collect(Collectors.toMap(key, holder -> holder));
        
        this.holderIO = holderIO;
        this.key = key;
    }
    
    public void add(V holder) {
        this.holders.put(key.apply(holder), holder);
        this.holderIO.register(holder);
    }
    
    public V remove(K key) {
        val holder = this.holders.remove(key);
        if (holder == null) return null;
        this.holderIO.unregister(holder);
        return holder;
    }
    
    public V get(K key) {
        return this.holders.get(key);
    }
    
    public Set<K> keySet() {
        return this.holders.keySet();
    }
    
    public Collection<V> values() {
        return this.holders.values();
    }
}
