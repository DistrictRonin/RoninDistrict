package de.ronin.ronindistrict.holder;

import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolderDeserializer;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolderDeserializer;
import de.ronin.ronindistrict.holder.restore.RestoreChunkHolder;
import de.ronin.ronindistrict.holder.restore.RestoreChunkHolderDeserializer;
import de.ronin.ronindistrict.holder.server.ServerChunkHolder;
import de.ronin.ronindistrict.holder.server.ServerChunkHolderDeserializer;
import de.ronin.ronindistrict.holder.spawn.SpawnChunkHolder;
import de.ronin.ronindistrict.holder.spawn.SpawnChunkHolderDeserializer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

@FieldDefaults(makeFinal = true)
public class ChunkHolderIO implements Runnable {

    private DistrictWorld world;
    @Getter private File holderFolder;
    
    private WeakHashMap<ChunkHolder, Void> holders;
    private Map<Class<? extends ChunkHolder>, ChunkHolderDeserializer<? extends ChunkHolder>> deserializer;
    
    private ScheduledExecutorService executor;
    
    public ChunkHolderIO(DistrictWorld world) {
        this.world = world;
        this.holderFolder = new File(this.world.getWorldFolder(), "RoninDistrict/holder");
        this.holderFolder.mkdirs();
        this.holders = new WeakHashMap<>();
        this.deserializer = new HashMap<>();
        this.deserializer.put(ServerChunkHolder.class, new ServerChunkHolderDeserializer());
        this.deserializer.put(RestoreChunkHolder.class, new RestoreChunkHolderDeserializer());
        this.deserializer.put(SpawnChunkHolder.class, new SpawnChunkHolderDeserializer());
        this.deserializer.put(PlotChunkHolder.class, new PlotChunkHolderDeserializer());
        this.deserializer.put(ProtectionChunkHolder.class, new ProtectionChunkHolderDeserializer());
    
        this.executor = Executors.newSingleThreadScheduledExecutor();
        this.executor.scheduleAtFixedRate(this, 1L, 1L, TimeUnit.HOURS);
    }
    
    public void register(ChunkHolder holder) {
        synchronized (this.holders) {
            this.holders.put(holder, null);
            File holderFile = new File(this.holderFolder, holder.getId());
            try {
                holderFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void unregister(ChunkHolder holder) {
        synchronized (this.holders) {
            this.holders.remove(holder);
            File holderFile = new File(this.holderFolder, holder.getId());
            holderFile.delete();
        }
    }
    
    public <T extends ChunkHolder> CompletableFuture<T> load(Class<T> clazz, String id) {
        return CompletableFuture.supplyAsync(() -> {
            File holderFile = new File(this.holderFolder, id);
            val deserializer = this.deserializer.get(clazz);
            //noinspection unchecked
            val holder = (T) deserializer.read(holderFile, this.world);
            synchronized (this.holders) {
                this.holders.put(holder, null);
            }
            return holder;
        });
    }
    
    public <T extends ChunkHolder> CompletableFuture<List<T>> load(Class<T> clazz, FilenameFilter filter) {
        return CompletableFuture.supplyAsync(() -> {
            val list = new ArrayList<T>();
            val files = this.holderFolder.listFiles(filter);
            assert files != null;
            for (File file : files) {
                try {
                    list.add(this.load(clazz, file.getName()).get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return list;
        });
    }
    
    public boolean hasData(String id) {
        File holderFile = new File(this.holderFolder, id);
        return holderFile.exists();
    }
    
    @Override
    @SneakyThrows
    public void run() {
        System.out.println("Saving " + this.holders.size() + " holders in world " + world.getName() + "...");
        CompletableFuture[] futures = new CompletableFuture[this.holders.size()];
        synchronized (this.holders) {
            int i = 0;
            for (ChunkHolder holder : this.holders.keySet()) {
                futures[i] = holder.serialize();
                i++;
            }
        }
        
        CompletableFuture.allOf(futures).get();
        System.out.println("Saved in world " + world.getName() + "...");
    }
    
    public CompletableFuture<Void> shutdown() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                this.executor.shutdown();
                this.executor.awaitTermination(1L, TimeUnit.DAYS);
                run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        });
    }
}
