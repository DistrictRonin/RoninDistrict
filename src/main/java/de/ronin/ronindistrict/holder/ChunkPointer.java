package de.ronin.ronindistrict.holder;

import lombok.Value;

@Value
public class ChunkPointer {
    int chunkX, chunkZ;
}
