package de.ronin.ronindistrict.holder.protection;

import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class ProtectionChunkHolder<T extends ChunkHolder & ChunkHolderProtectible> extends ChunkHolder {
    
    public static final String ID = "ProtectionChunk_";
    
    @Getter private T target;
    
    public ProtectionChunkHolder(DistrictWorld world, T target) {
        super(ID + target.getId(), world, new ProtectionChunkHolderSerializer());
        this.target = target;
    }
    
    public CompletableFuture<Void> setRadius(ChunkPointer center, int radius) {
        val leftToDeassign = new HashSet<>(this.getChunks().values());
        
        val futures = new ArrayList<CompletableFuture>();
        for (int dY = -radius; dY <= radius; dY++) {
            for (int dX = -radius; dX <= radius; dX++) {
                if (dX * dX + dY * dY <= radius * radius) {
                    val pointer = new ChunkPointer(
                            center.getChunkX() + dX,
                            center.getChunkZ() + dY
                    );
                    if (!leftToDeassign.remove(pointer)) {
                        futures.add(this.assignProtection(pointer));
                    }
                }
            }
        }
        
        leftToDeassign.forEach(pointer -> this.getWorld().queueDeassignment(pointer));
        
        val futArray = new CompletableFuture[futures.size()];
        futures.toArray(futArray);
        return CompletableFuture.allOf(futArray);
    }
    
    public CompletableFuture<Void> setBox(ChunkPointer center, int radius) {
        val leftToDeassign = new HashSet<>(this.getChunks().values());
    
        val futures = new ArrayList<CompletableFuture>();
        for (int dY = -radius; dY <= radius; dY++) {
            for (int dX = -radius; dX <= radius; dX++) {
                val pointer = new ChunkPointer(
                        center.getChunkX() + dX,
                        center.getChunkZ() + dY
                );
                if (!leftToDeassign.remove(pointer)) {
                    futures.add(this.assignProtection(pointer));
                }
            }
        }
    
        leftToDeassign.forEach(pointer -> this.getWorld().queueDeassignment(pointer));
    
        val futArray = new CompletableFuture[futures.size()];
        futures.toArray(futArray);
        return CompletableFuture.allOf(futArray);
    }
    
    private void addIfFree(Set<ChunkPointer> set, ChunkPointer center, int xOff, int zOff) {
        val pointer = new ChunkPointer(center.getChunkX() + xOff, center.getChunkZ() + zOff);
        if (!this.getWorld().getHolders().containsKey(pointer)) {
            set.add(pointer);
        }
    }
    
    public Set<ChunkPointer> radiateOutwards() {
        val newlyAcquired = new HashSet<ChunkPointer>();
        for (ChunkPointer pointer : this.getChunks().values()) {
            this.addIfFree(newlyAcquired, pointer,  1,  0);
            this.addIfFree(newlyAcquired, pointer, -1,  0);
            this.addIfFree(newlyAcquired, pointer,  0,  1);
            this.addIfFree(newlyAcquired, pointer,  0, -1);
        }
        
        return newlyAcquired;
    }
    
    /** Only areas that aren't otherwise occupied can be protected */
    private CompletableFuture assignProtection(ChunkPointer pointer) {
        val holder = this.getWorld().getHolders().get(pointer);
        if (holder == null) {
            return this.getWorld().assignHolder(pointer, this);
        }
        return CompletableFuture.completedFuture(null);
    }
    
    @Override
    protected void onAdd(ChunkPointer pointer) { }
    
    @Override
    protected void onRemove(ChunkPointer pointer) { }
    
    @Override
    protected List<String> holderInfo() {
        val info = new ArrayList<String>();
        info.add("\u00a77---- \u00a7Protection Chunk \u00a77----");
        info.add("\u00a77Reserviert für: " + this.target.displayName() + " [" + this.target.getId() + "]");
        info.add("\u00a77Chunks: " + String.join(", ",
                this.getChunks().values().stream()
                        .map(pointer -> pointer.getChunkX() + "|" + pointer.getChunkZ())
                        .collect(Collectors.toSet())
        ));
        return info;
    }
    
    @Override
    public Optional<String> hudDisplay() {
        return Optional.of("\u00a7cReserviert für " + this.target.displayName());
    }
}
