package de.ronin.ronindistrict.holder.protection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.ronin.ronindistrict.holder.ChunkHolderSerializer;
import lombok.val;

import java.io.File;

public class ProtectionChunkHolderSerializer implements ChunkHolderSerializer<ProtectionChunkHolder<?>> {
    
    private File file;
    private ProtectionChunkHolder<?> holder;
    
    private final Gson gson = new Gson();
    
    @Override
    public void init(File file, ProtectionChunkHolder<?> holder) {
        this.file = file;
        this.holder = holder;
    }
    
    @Override
    public void write() {
        val root = new JsonObject();
        root.addProperty("target", this.holder.getTarget().key());
        root.add("chunks", this.writeChunks(this.holder));
        this.writeFile(this.gson, this.file, root);
    }
}
