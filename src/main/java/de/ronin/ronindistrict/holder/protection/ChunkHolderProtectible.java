package de.ronin.ronindistrict.holder.protection;

import java.util.UUID;

public interface ChunkHolderProtectible {

    String key();
    String displayName();
    boolean isOwner(UUID player);
}
