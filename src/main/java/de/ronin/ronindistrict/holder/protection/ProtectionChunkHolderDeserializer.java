package de.ronin.ronindistrict.holder.protection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.ronin.ronindistrict.holder.ChunkHolderDeserializer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.io.FileReader;
import java.util.UUID;

public class ProtectionChunkHolderDeserializer implements ChunkHolderDeserializer<ProtectionChunkHolder<?>> {
    
    private final Gson gson = new Gson();
    
    @Override
    @SneakyThrows
    public ProtectionChunkHolder read(File file, DistrictWorld world) {
        val root = gson.fromJson(new FileReader(file), JsonObject.class);
        
        val target = root.get("target").getAsString();
        val holder = this.createHolder(world, target);
        
        this.readChunks(root.get("chunks").getAsJsonArray(), world, holder);
        return holder;
    }
    
    private ProtectionChunkHolder<?> createHolder(DistrictWorld world, String key) {
        val spawnChunkHolder = world.getSpawnChunkHolder().get(key);
        if (spawnChunkHolder != null) {
            return new ProtectionChunkHolder<>(world, spawnChunkHolder);
        }
        
        try {
            val plotChunkHolder = world.getPlotChunkHolder().get(UUID.fromString(key));
            if (plotChunkHolder != null) {
                return new ProtectionChunkHolder<>(world, plotChunkHolder);
            }
        } catch (IllegalArgumentException e) { /* just an failed attempt to retrieve a valid chunk holder */ }
    
        throw new RuntimeException("Failed to resolve an ChunkHolder target for a ProtectionChunkHolder. " + key);
    }
}
