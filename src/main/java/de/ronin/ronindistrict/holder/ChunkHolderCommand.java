package de.ronin.ronindistrict.holder;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.entity.Player;

@FieldDefaults(makeFinal = true)
public class ChunkHolderCommand {
    
    @Command("district.chunk.info")
    public void chunkInfo(Player player, @Optional("$self") DistrictWorld world) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
    
        val holder = world.getHolders().get(pointer);
        if (holder != null) {
            holder.holderInfo().forEach(player::sendMessage);
        } else {
            player.sendMessage("\u00a7cThis chunk is not managed by anything.");
        }
    }
    
    @Command("district.chunk.return")
    public void chunkReturn(Player player, @Optional("$self") DistrictWorld world) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
    
        val future = world.queueDeassignment(pointer);
        player.sendMessage("\u00a7cReturn chunk to the unused pool. Queueing for reset");
    }
}
