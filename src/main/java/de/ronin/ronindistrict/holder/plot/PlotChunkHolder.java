package de.ronin.ronindistrict.holder.plot;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.protection.ChunkHolderProtectible;
import de.ronin.ronindistrict.plot.Plot;
import de.ronin.ronindistrict.util.PlayerNameUtil;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class PlotChunkHolder extends ChunkHolder implements ChunkHolderProtectible {
    
    public static final String ID = "PlotChunk_";
    
    private static final String BYPASS_BAN = "ronindistrict.plot.bypass_ban";
    
    @Getter private Plot plot;
    
    private Map<ChunkPointer, ProtectedRegion> regions;
    
    public PlotChunkHolder(DistrictWorld world, Plot plot) {
        super(ID + plot.getOwner().toString(), world, new PlotChunkHolderSerializer());
        this.plot = plot;
        this.regions = new HashMap<>();
    }
    
    @Override
    protected void onAdd(ChunkPointer pointer) {
        val x = pointer.getChunkX() * 16;
        val z = pointer.getChunkZ() * 16;
    
        val region = new ProtectedCuboidRegion("plot_" + this.plot.getOwner().toString() + "_" + pointer.getChunkX() + "_" + pointer.getChunkZ(), true,
                BlockVector3.at(x, 0, z), BlockVector3.at(x + 15, 255, z + 15));
    
        this.getWorld().getRegionManager().addRegion(region);
        this.regions.put(pointer, region);
    
        this.update();
    }
    
    @Override
    protected void onRemove(ChunkPointer pointer) {
        String id = "plot_" + this.plot.getOwner().toString() + "_" + pointer.getChunkX() + "_" + pointer.getChunkZ();
        this.regions.remove(pointer);
        this.getWorld().getRegionManager().removeRegion(id);
        
        this.update();
    }
    
    @Override
    @SneakyThrows
    protected List<String> holderInfo() {
        val info = new ArrayList<String>();
        val ownerName = PlayerNameUtil.requestName(this.plot.getOwner());
        val helperUUIDs = new UUID[this.plot.getHelpers().size()];
        val helperNames = PlayerNameUtil.requestName(this.plot.getHelpers().toArray(helperUUIDs));
        val bannedUUIDs = new UUID[this.plot.getBanned().size()];
        val bannedNames = PlayerNameUtil.requestName(this.plot.getBanned().toArray(bannedUUIDs));
        info.add("\u00a77---- \u00a76" + ownerName + "s Grundstück \u00a77----");
        info.add("\u00a77Helfer: " + String.join(", ", helperNames));
        info.add("\u00a77Hausverbote: " + String.join(", ", bannedNames));
        info.add("\u00a77Level: " + this.plot.getLevel());
        info.add("\u00a77Chunks: " + String.join(", ",
                this.getChunks().values().stream()
                        .map(pointer -> pointer.getChunkX() + "|" + pointer.getChunkZ())
                        .collect(Collectors.toSet())));
        return info;
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getPlayer().getLocation().getWorld().getUID().equals(this.getWorld().getUID())) {
            return;
        }
        
        val previous = e.getFrom().getChunk();
        if (e.getTo() == null) return;
        val next = e.getTo().getChunk();
        if (this.getChunks().contains(next.getX(), next.getZ())) {
            if (!e.getPlayer().hasPermission(BYPASS_BAN)) {
                if (this.getPlot().getBanned().contains(e.getPlayer().getUniqueId())) {
                    org.bukkit.util.Vector pos = e.getPlayer().getLocation().toVector();
                    org.bukkit.util.Vector toMid = new org.bukkit.util.Vector(next.getX() * 16 + 7.5, pos.getY(), next.getZ() * 16 + 7.5).subtract(pos);
                    org.bukkit.util.Vector fromMid = toMid.normalize().multiply(-1);
                    e.getPlayer().setVelocity(fromMid.add(new Vector(0, 0.1, 0)).multiply(2f));
                    e.getPlayer().sendMessage("\u00a7cDu hast auf diesem Grundstück Hausverbot.");
                }
            }
            
            if (previous != next && !this.getChunks().contains(previous.getX(), previous.getZ())) {
                val name = PlayerNameUtil.requestName(this.getPlot().getOwner());
                e.getPlayer().sendMessage("\u00a77" + name + "s Grundstück");
            }
        }
    }
    
    public void update() {
        for (ProtectedRegion region : this.regions.values()) {
            region.getMembers().clear();
            region.getMembers().addPlayer(this.getPlot().getOwner());
            for (UUID helper : this.getPlot().getHelpers()) {
                region.getMembers().addPlayer(helper);
            }
        }
    }
    
    @Override
    public void destroy() {
        super.destroy();
        val protection = this.getWorld().getProtectionChunkHolder().remove(this.getId());
        val values = new HashSet<>(protection.getChunks().values());
        for (ChunkPointer pointer : values) {
            this.getWorld().queueDeassignment(pointer);
        }
        protection.destroy();
    }
    
    @Override
    public String key() {
        return this.plot.getOwner().toString();
    }
    
    @Override
    @SneakyThrows
    public String displayName() {
        return PlayerNameUtil.requestName(this.plot.getOwner());
    }
    
    @Override
    public boolean isOwner(UUID player) {
        return this.plot.getOwner().equals(player);
    }
}
