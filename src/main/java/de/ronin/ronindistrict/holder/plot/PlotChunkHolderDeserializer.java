package de.ronin.ronindistrict.holder.plot;

import com.google.common.collect.Streams;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkHolderDeserializer;
import de.ronin.ronindistrict.level.LevelProgress;
import de.ronin.ronindistrict.plot.Plot;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.io.File;
import java.io.FileReader;
import java.util.UUID;
import java.util.stream.Collectors;

public class PlotChunkHolderDeserializer implements ChunkHolderDeserializer<PlotChunkHolder> {
    
    private final Gson gson = new Gson();
    
    @Override
    @SneakyThrows
    public PlotChunkHolder read(File file, DistrictWorld world) {
        val root = gson.fromJson(new FileReader(file), JsonObject.class);
        
        val owner = UUID.fromString(root.get("owner").getAsString());
        val helpers = Streams.stream(root.get("helpers").getAsJsonArray())
                .map(uuidElem -> UUID.fromString(uuidElem.getAsString()))
                .collect(Collectors.toSet());
        val banned = Streams.stream(root.get("banned").getAsJsonArray())
                .map(uuidElem -> UUID.fromString(uuidElem.getAsString()))
                .collect(Collectors.toSet());
        val loc = root.get("home").getAsJsonArray();
        val home = new Location(Bukkit.getWorld(world.getUID()),
                loc.get(0).getAsDouble(), loc.get(1).getAsDouble(), loc.get(2).getAsDouble(),
                loc.get(3).getAsFloat(), loc.get(4).getAsFloat());
        val level = root.get("level").getAsInt();
        
        val delivered = root.get("delivered").getAsJsonObject().entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> Material.getMaterial(entry.getKey()),
                        entry -> entry.getValue().getAsInt())
                );
    
        val protectionExpansionsElem = root.get("protectionExpansions");
        int expansions = 0;
        if (protectionExpansionsElem != null) {
            expansions = protectionExpansionsElem.getAsInt();
        }
        
        val plot = new Plot(world, owner, helpers, banned, home, level, expansions);
        while (RoninDistrict.getInstance().getLevelCache() == null); // FIXME: busy-waiting is always bad
        val levelInst = RoninDistrict.getInstance().getLevelCache().getLevels().get(world, plot.getLevel());
        if (levelInst != null) {
            plot.setProgress(new LevelProgress(
                    plot,
                    levelInst,
                    delivered
            ));
        }
        val holder = new PlotChunkHolder(world, plot);
        
        this.readChunks(root.get("chunks").getAsJsonArray(), world, holder);
        
        return holder;
    }
}
