package de.ronin.ronindistrict.holder.plot;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import de.ronin.ronindistrict.holder.ChunkHolderSerializer;
import lombok.val;

import java.io.File;

public class PlotChunkHolderSerializer implements ChunkHolderSerializer<PlotChunkHolder> {
    
    private File file;
    private PlotChunkHolder holder;
    
    private final Gson gson = new Gson();
    
    @Override
    public void init(File file, PlotChunkHolder holder) {
        this.file = file;
        this.holder = holder;
    }
    
    @Override
    public void write() {
        val helpers = new JsonArray();
        this.holder.getPlot().getHelpers().forEach(uuid -> helpers.add(uuid.toString()));
        val banned = new JsonArray();
        this.holder.getPlot().getBanned().forEach(uuid -> banned.add(uuid.toString()));
        val home = new JsonArray();
        val loc = this.holder.getPlot().getHome();
        home.add(loc.getX()); home.add(loc.getY()); home.add(loc.getZ());
        home.add(loc.getYaw()); home.add(loc.getPitch());
    
        val delivered = new JsonObject();
        if (this.holder.getPlot().getProgress() != null) {
            this.holder.getPlot().getProgress().getDelivered().forEach((mat, count) -> {
                delivered.addProperty(mat.name(), count);
            });
        }
        
        val root = new JsonObject();
        root.addProperty("owner", this.holder.getPlot().getOwner().toString());
        root.add("helpers", helpers);
        root.add("banned", banned);
        root.add("home", home);
        root.addProperty("level", this.holder.getPlot().getLevel());
        root.add("delivered", delivered);
        root.addProperty("protectionExpansions", this.holder.getPlot().getProtectionExpansions());
        root.add("chunks", this.writeChunks(this.holder));
        
        this.writeFile(gson, this.file, root);
    }
}
