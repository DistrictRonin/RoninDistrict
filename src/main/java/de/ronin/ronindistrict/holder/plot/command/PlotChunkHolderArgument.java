package de.ronin.ronindistrict.holder.plot.command;

import de.ostylk.baseapi.modules.command.argument.ArgumentParser;
import de.ostylk.baseapi.modules.command.argument.ParsePayload;
import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.util.PlayerNameUtil;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PlotChunkHolderArgument implements ArgumentParser<PlotChunkHolder> {
    
    @SettingLoad("default_world")
    public static String DEFAULT_WORLD = "RoninDistrict";
    
    @Override
    @SneakyThrows
    public Set<String> getAllPossibilities() {
        val set = new HashSet<String>();
        for (DistrictWorld world : RoninDistrict.getInstance().getWorldCache().getWorlds().values()) {
            for (UUID uuid : world.getPlotChunkHolder().keySet()) {
                val playerName = PlayerNameUtil.requestName(uuid);
                set.add(playerName + ":" + world.getName());
            }
        }
        return set;
    }
    
    @Override
    public ParsePayload<PlotChunkHolder> parse(CommandSender sender, String arg) {
        if (arg.equals("$self") && sender instanceof Player) {
            val holder = this.getAtPlayerPosition((Player) sender);
            if (holder != null) {
                return ParsePayload.success(holder);
            }
            return this.parse(sender, sender.getName(), DEFAULT_WORLD);
        } else {
            val args = arg.split(":");
            return this.parse(sender,
                    args[0].isEmpty() ? sender.getName() : args[0],
                    args.length <= 1 || args[1].isEmpty() ? this.getSenderWorld(sender) : args[1]
            );
        }
    }

    private PlotChunkHolder getAtPlayerPosition(Player player) {
        val world = RoninDistrict.getInstance().getWorldCache().getWorlds().get(player.getWorld().getUID());
        if (world == null) {
            return null;
        }

        val pointer = new ChunkPointer(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ());
        val holder = world.getHolders().get(pointer);
        if (holder == null) {
            return null;
        }

        if (holder instanceof PlotChunkHolder) {
            return (PlotChunkHolder) holder;
        } else if (holder instanceof ProtectionChunkHolder<?>) {
            val protector = (ProtectionChunkHolder<?>) holder;
            if (protector.getTarget() instanceof PlotChunkHolder) {
                return ((PlotChunkHolder) protector.getTarget());
            }
        }
        return null;
    }

    private String getSenderWorld(CommandSender sender) {
        if (sender instanceof Player) {
            val player = (Player) sender;
            val world = RoninDistrict.getInstance().getWorldCache().getWorlds().get(player.getLocation().getWorld().getUID());
            if (world != null) {
                return world.getName();
            } else {
                return DEFAULT_WORLD;
            }
        }
        return DEFAULT_WORLD;
    }
    
    private ParsePayload<PlotChunkHolder> parse(CommandSender sender, String targetName, String worldName) {
        UUID target;
        try {
            target = RoninDistrict.PLAYER_CACHE.lookupUUID(targetName).get(50L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return ParsePayload.error("Anfrage dauerte zu lange, versuche es nochmal.");
        }
        if (target == null) {
            return ParsePayload.error(targetName + " war nie auf dem Server");
        }
        
        val bukkitWorld = Bukkit.getWorld(worldName);
        if (bukkitWorld == null) {
            return ParsePayload.error(worldName + " ist keine Welt.");
        }
        
        val world = RoninDistrict.getInstance().getWorldCache().getWorlds().get(bukkitWorld.getUID());
        if (world == null) {
            return ParsePayload.error(worldName + " ist keine Distrikt-Welt.");
        }
        
        val holder = world.getPlotChunkHolder().get(target);
        if (holder == null) {
            return ParsePayload.error(targetName + " hat kein Grundstück in dieser Welt.");
        }
        
        return ParsePayload.success(holder);
    }
    
    @Override
    public Class<PlotChunkHolder> getType() {
        return PlotChunkHolder.class;
    }
}
