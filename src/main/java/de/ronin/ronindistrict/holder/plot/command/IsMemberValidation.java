package de.ronin.ronindistrict.holder.plot.command;

import de.ostylk.baseapi.modules.command.validator.ArgumentValidator;
import de.ostylk.baseapi.modules.command.validator.ValidationPayload;
import de.ronin.ronindistrict.holder.plot.PlotChunkHolder;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.lang.annotation.*;

public class IsMemberValidation implements ArgumentValidator<PlotChunkHolder> {
    @Override
    public ValidationPayload validate(CommandSender sender, PlotChunkHolder holder, Annotation validationAnnotation) {
        if (sender.hasPermission("ronindistrict.plot.force.member")) return ValidationPayload.success();

        if (sender instanceof ConsoleCommandSender) {
            return ValidationPayload.success();
        } else if (sender instanceof Player) {
            Player player = (Player) sender;

            if (holder.getPlot().getOwner().equals(player.getUniqueId()) ||
                holder.getPlot().getHelpers().contains(player.getUniqueId())) {
                return ValidationPayload.success();
            } else {
                return ValidationPayload.error("Du bist kein Helfer auf diesem Grundstück.");
            }
        }
        return ValidationPayload.error("Unbekannter Fehler.");
    }

    @Override
    public Class<? extends Annotation> validatorAnnotation() {
        return IsMember.class;
    }

    @Override
    public Class<PlotChunkHolder> argumentType() {
        return PlotChunkHolder.class;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface IsMember {}
}
