package de.ronin.ronindistrict.holder.spawn;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import de.ronin.ronindistrict.holder.ChunkHolderSerializer;
import lombok.val;

import java.io.File;

public class SpawnChunkHolderSerializer implements ChunkHolderSerializer<SpawnChunkHolder> {
    
    private File file;
    private SpawnChunkHolder holder;
    
    private final Gson gson = new Gson();
    
    @Override
    public void init(File file, SpawnChunkHolder holder) {
        this.file = file;
        this.holder = holder;
    }
    
    @Override
    public void write() {
        val root = new JsonObject();
        root.add("displayName", new JsonPrimitive(this.holder.getDisplayName()));
        root.add("chunks", this.writeChunks(this.holder));
        
        val location = new JsonArray();
        location.add(this.holder.getSpawn().getX());
        location.add(this.holder.getSpawn().getY());
        location.add(this.holder.getSpawn().getZ());
        location.add(this.holder.getSpawn().getPitch());
        location.add(this.holder.getSpawn().getYaw());
        
        root.add("location", location);
        
        this.writeFile(this.gson, this.file, root);
    }
}
