package de.ronin.ronindistrict.holder.spawn;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class SpawnChunkHolderCommand {

    @Command("district.spawn.create")
    public void chunkSpawnCreate(Player player, String name, String displayName, @Optional("$self") DistrictWorld world) {
        val pointer = new ChunkPointer(
                player.getLocation().getChunk().getX(),
                player.getLocation().getChunk().getZ()
        );
        
        val holder = new SpawnChunkHolder(
                world,
                name,
                ChatColor.translateAlternateColorCodes('&', displayName),
                player.getLocation()
        );
        world.getSpawnChunkHolder().add(holder);
        world.assignHolder(pointer, holder);
        player.sendMessage("\u00a7aSpawn erstellt.");
    }
    
    @Command("district.spawn.destroy")
    public void chunkSpawnDestroy(CommandSender sender, SpawnChunkHolder spawn) {
        val values = new HashSet<>(spawn.getChunks().values());
        for (ChunkPointer pointer : values) {
            spawn.getWorld().queueDeassignment(pointer);
        }
        spawn.destroy();
        sender.sendMessage("\u00a7cSpawn zerstört. Alle Chunks wurden zum Reset markiert.");
    }
    
    @Command("district.spawn.add")
    public void chunkSpawnAdd(Player player, SpawnChunkHolder spawn) {
        val pointer = new ChunkPointer(
            player.getLocation().getChunk().getX(),
            player.getLocation().getChunk().getZ()
        );
        
        if (!spawn.getChunks().contains(pointer.getChunkX(), pointer.getChunkZ())) {
            spawn.getWorld().assignHolder(pointer, spawn);
            player.sendMessage("\u00a7aChunk zum Spawn hinzugefügt.");
        } else {
            player.sendMessage("\u00a7cDieser Chunk gehört bereits zu diesem Spawn.");
        }
    }
    
    @Command("district.spawn.location")
    public void chunkSpawnLocation(Player player, SpawnChunkHolder spawn) {
        spawn.setSpawn(player.getLocation());
        player.sendMessage("\u00a7aNeue Spawn-Location gesetzt.");
    }
    
    @Command("district.spawn.displayname")
    public void chunkSpawnDisplayName(CommandSender sender, SpawnChunkHolder spawn, String displayName) {
        spawn.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
        sender.sendMessage("\u00a7aNeuer Anzeigename des Spawns gesetzt.");
    }
    
    @Command("district.spawn.teleport")
    public void chunkSpawnTeleport(CommandSender sender, SpawnChunkHolder spawn, @Optional("$self") Player target) {
        target.teleport(spawn.getSpawn());
    }
    
    @Command("district.spawn.protectionradius")
    public void chunkSpawnRadius(CommandSender sender, SpawnChunkHolder spawn, int radius) {
        spawn.setProtectionRadius(radius);
        sender.sendMessage("\u00a7aSchutzradius für den Spawn wurde gesetzt.");
    }
}
