package de.ronin.ronindistrict.holder.spawn;

import de.ostylk.baseapi.modules.command.argument.ArgumentParser;
import de.ostylk.baseapi.modules.command.argument.ParsePayload;
import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class SpawnChunkHolderArgument implements ArgumentParser<SpawnChunkHolder> {
    
    @SettingLoad("default_world")
    public static String DEFAULT_WORLD = "RoninDistrict";
    
    @Override
    public Set<String> getAllPossibilities() {
        val set = new HashSet<String>();
        for (DistrictWorld world : RoninDistrict.getInstance().getWorldCache().getWorlds().values()) {
            for (String spawn : world.getSpawnChunkHolder().keySet()) {
                set.add(spawn + ":" + world.getName());
            }
        }
        return set;
    }
    
    @Override
    public ParsePayload<SpawnChunkHolder> parse(CommandSender sender, String arg) {
        val args = arg.split(":");
        return this.parse(
                args[0],
                args.length <= 1 || args[1].isEmpty() ? this.getPlayerWorld(sender) : args[1]
        );
    }
    
    private String getPlayerWorld(CommandSender sender) {
        if (sender instanceof Player) {
            val player = (Player) sender;
            val world = RoninDistrict.getInstance().getWorldCache().getWorlds().get(player.getLocation().getWorld().getUID());
            if (world != null) {
                return world.getName();
            } else {
                return DEFAULT_WORLD;
            }
        }
        return DEFAULT_WORLD;
    }
    
    private ParsePayload<SpawnChunkHolder> parse(String spawnName, String worldName) {
        val bukkitWorld = Bukkit.getWorld(worldName);
        if (bukkitWorld == null) {
            return ParsePayload.error(worldName + " ist keine Welt.");
        }
    
        val world = RoninDistrict.getInstance().getWorldCache().getWorlds().get(bukkitWorld.getUID());
        if (world == null) {
            return ParsePayload.error(worldName + " ist keine Distrikt-Welt.");
        }
        
        val holder = world.getSpawnChunkHolder().get(spawnName);
        if (holder == null) {
            return ParsePayload.error(spawnName + " ist kein Spawn in dieser Welt.");
        }
        
        return ParsePayload.success(holder);
    }
    
    @Override
    public Class<SpawnChunkHolder> getType() {
        return SpawnChunkHolder.class;
    }
}
