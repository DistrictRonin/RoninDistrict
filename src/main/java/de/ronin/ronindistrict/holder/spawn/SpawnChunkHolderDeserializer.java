package de.ronin.ronindistrict.holder.spawn;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.ronin.ronindistrict.holder.ChunkHolderDeserializer;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.io.FileReader;

public class SpawnChunkHolderDeserializer implements ChunkHolderDeserializer<SpawnChunkHolder> {
    
    private final Gson gson = new Gson();
    
    @Override
    @SneakyThrows
    public SpawnChunkHolder read(File file, DistrictWorld world) {
        val root = gson.fromJson(new FileReader(file), JsonObject.class);
        
        val name = file.getName().substring(SpawnChunkHolder.ID.length());
        val displayName = root.get("displayName").getAsString();
        val locationArray = root.get("location").getAsJsonArray();
        val location = new Location(Bukkit.getWorld(world.getUID()),
                locationArray.get(0).getAsDouble(), locationArray.get(1).getAsDouble(), locationArray.get(2).getAsDouble(),
                locationArray.get(4).getAsFloat(), locationArray.get(3).getAsFloat());
        
        val holder = new SpawnChunkHolder(world, name, displayName, location);
        
        val chunks = root.get("chunks").getAsJsonArray();
        this.readChunks(chunks, world, holder);
        return holder;
    }
}
