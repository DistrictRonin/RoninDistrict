package de.ronin.ronindistrict.holder.spawn;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.ronin.ronindistrict.RoninDistrict;
import de.ronin.ronindistrict.holder.ChunkHolder;
import de.ronin.ronindistrict.holder.ChunkPointer;
import de.ronin.ronindistrict.holder.protection.ChunkHolderProtectible;
import de.ronin.ronindistrict.holder.protection.ProtectionChunkHolder;
import de.ronin.ronindistrict.world.DistrictWorld;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.val;
import lombok.var;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;
import java.util.stream.Collectors;

@FieldDefaults(makeFinal = true)
public class SpawnChunkHolder extends ChunkHolder implements ChunkHolderProtectible {
    
    public static final String ID = "SpawnChunk_";
    private static final String MODIFY_PERMISSION = "ronindistrict.chunk.spawn.modify";
    
    @Getter private String name;
    @NonFinal @Getter @Setter private String displayName;
    @NonFinal @Getter @Setter private Location spawn;
    
    private Map<ChunkPointer, ProtectedRegion> regions;
    
    public SpawnChunkHolder(DistrictWorld world, String name, String displayName, Location spawn) {
        super(ID + name, world, new SpawnChunkHolderSerializer());
        this.name = name;
        this.displayName = displayName;
        this.spawn = spawn;
        
        this.regions = new HashMap<>();
    }
    
    @Override
    protected void onAdd(ChunkPointer pointer) {
        val x = pointer.getChunkX() * 16;
        val z = pointer.getChunkZ() * 16;
    
        val region = new ProtectedCuboidRegion("spawn_" + name + "_" + pointer.getChunkX() + "_" + pointer.getChunkZ(), true,
                BlockVector3.at(x, 0, z), BlockVector3.at(x + 15, 255, z + 15));
    
        this.getWorld().getRegionManager().addRegion(region);
        this.regions.put(pointer, region);
    }
    
    @Override
    protected void onRemove(ChunkPointer pointer) {
        String id = "spawn_" + name + "_" + pointer.getChunkX() + "_" + pointer.getChunkZ();
        this.regions.remove(pointer);
        this.getWorld().getRegionManager().removeRegion(id);
    }
    
    public void setProtectionRadius(int radius) {
        var midX = 0.0;
        var midZ = 0.0;
        for (ChunkPointer pointer : this.getChunks().values()) {
            midX += pointer.getChunkX();
            midZ += pointer.getChunkZ();
        }
        ChunkPointer midPointer = new ChunkPointer(
                (int) (midX / this.getChunks().size()),
                (int) (midZ / this.getChunks().size())
        );
    
        var holder = this.getWorld().getProtectionChunkHolder().get(this.getId());
        if (holder == null) {
            holder = new ProtectionChunkHolder<>(this.getWorld(), this);
            this.getWorld().getProtectionChunkHolder().add(holder);
        }
        holder.setRadius(midPointer, radius);
    }
    
    @Override
    protected List<String> holderInfo() {
        val info = new ArrayList<String>();
        info.add("\u00a77---- " + this.displayName + " \u00a77----");
        info.add("\u00a77Name: " + this.getName());
        info.add("\u00a77Chunks: " + String.join(", ",
                this.getChunks().values().stream()
                        .map(pointer -> pointer.getChunkX() + "|" + pointer.getChunkZ())
                        .collect(Collectors.toSet())));
        return info;
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getPlayer().getLocation().getWorld().getUID().equals(this.getWorld().getUID())) {
            return;
        }
        
        val previous = e.getFrom().getChunk();
        if (e.getTo() == null) return;
        val next = e.getTo().getChunk();
        if (previous != next) {
            // Called when stepping first time into spawn
            if (this.getChunks().contains(next.getX(), next.getZ())
                    && !this.getChunks().contains(previous.getX(), previous.getZ())) {
                e.getPlayer().sendMessage(this.displayName + " betreten");
            }
        }
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (e.getPlayer().hasPermission(MODIFY_PERMISSION)) {
            RoninDistrict.CONCURRENT.submitTask(() -> {
                this.regions.values().forEach(region -> region.getMembers().addPlayer(e.getPlayer().getUniqueId()));
            });
        }
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        RoninDistrict.CONCURRENT.submitTask(() -> {
            this.regions.values().forEach(region -> region.getMembers().removePlayer(e.getPlayer().getUniqueId()));
        });
    }
    
    @Override
    public String key() {
        return this.name;
    }
    
    @Override
    public String displayName() {
        return this.displayName;
    }
    
    @Override
    public boolean isOwner(UUID player) {
        return false;
    }
}
