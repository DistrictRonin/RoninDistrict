package de.ronin.ronindistrict.holder;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.io.FileWriter;

public interface ChunkHolderSerializer<T extends ChunkHolder> {

    void init(File file, T holder);
    void write();
    
    default JsonArray writeChunks(T holder) {
        val chunks = new JsonArray();
        for (ChunkPointer pointer : holder.getChunks().values()) {
            val array = new JsonArray();
            array.add(pointer.getChunkX());
            array.add(pointer.getChunkZ());
            chunks.add(array);
        }
        return chunks;
    }
    
    @SneakyThrows
    default void writeFile(Gson gson, File file, JsonElement json) {
        val writer = new FileWriter(file, false);
        writer.write(gson.toJson(json));
        writer.close();
    }
}
